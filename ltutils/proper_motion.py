#
# Classe per leggere, calcolare e plottare dati di proper motion
#

__author__ = 'ltesti - 2 Aug 2015'

import numpy as np
import os
import matplotlib.pyplot as plt
from astropy.time import Time
from astropy import units as u
from astropy.coordinates import SkyCoord


class ProperMotion(object):
    def __init__(self, filename):
    	self.infile = filename
    	self.__read_data()
    	self.nep = len(self.time)
    	self.ydiff = self.__calc_year_diff()
    	self.ad_pm, self.err_pm = self.__calc_pos_pm()
    	self.rhopa, self.rhopa_pm = self.__calc_rho_pa()
    	self.yeardec = self.__get_year_dec()

    # reads from data file
    def __read_data(self):
        openfile = open(self.infile, 'r')
        lines = openfile.readlines()
        openfile.close()

        self.time=[]; self.ad=[]; self.err=[]
        for j in xrange(len(lines)):
            tt, aa, dd, eaa, edd = lines[j].split()[0:5]
            self.time.append(Time(tt)); self.ad.append(SkyCoord(aa,dd))
            self.err.append([float(eaa),float(edd)])
            if j == 0:
                pma,epma,pmd,epmd = lines[j].split()[5:9]
                self.pm=(float(pma),float(epma),float(pmd),float(epmd))

    def __str__(self):
        rep = "Proper Motion data from file: " + self.infile

        return rep

    def __calc_pos_pm(self):
        #
        ad_pm = []; err_pm = []
        cosd = np.cos(self.ad[0].dec.value/180.*np.pi)
        for i in xrange(self.nep):
            if i == 0:
        	    err_pm.append(self.err[0])
        	    ad_pm.append(self.ad[0])
            else:
                pma = self.pm[0]*self.ydiff[i]/3600./1000.
                pmd = self.pm[2]*self.ydiff[i]/3600./1000.
                dv = self.ad[0].dec.value+pmd
                av = self.ad[0].ra.value-pma/cosd
                ad_pm.append(SkyCoord(av*u.deg,dv*u.deg))
                #epma = np.sqrt((self.pm[1]*self.ydiff[i])**2+(self.err[0][0])**2)
                #epmd = np.sqrt((self.pm[3]*self.ydiff[i])**2+(self.err[0][1])**2)
                epma = self.pm[1]*self.ydiff[i]+self.err[0][0]
                epmd = self.pm[3]*self.ydiff[i]+self.err[0][1]
                err_pm.append([epma,epmd])
        #
        return ad_pm, err_pm

    def __get_year_dec(self):
        #
        # computes the array of decimal years
        x = np.zeros(self.nep)
        for i in xrange(self.nep): 
            x[i]=self.time[i].decimalyear
        #
        return x

    def __calc_year_diff(self):
        #
        # compute a vector of decimal year differences
        tyd = np.zeros(self.nep)
        for i in xrange(self.nep):
            tyd[i]=self.time[i].decimalyear-self.time[0].decimalyear
        #
        return tyd
 
    def __get_sep(self,a,b):
    	#
    	return [a.separation(b).degree,a.position_angle(b).degree]

    def __calc_rho_pa(self):
        #
        sep = np.zeros((self.nep,2))
        sep_pm = np.zeros((self.nep,2))
        for i in xrange(self.nep):
            sep[i,:] = self.__get_sep(self.ad[0],self.ad[i])
            sep_pm[i,:] = self.__get_sep(self.ad_pm[0],self.ad_pm[i])
        #
        return sep, sep_pm

    def plot_rho_pa(self):
    	#
    	xpfac = 0.1
        xmin = self.yeardec[0]-(self.yeardec[self.nep-1]-self.yeardec[0])*xpfac
        xmax = self.yeardec[self.nep-1]+(self.yeardec[self.nep-1]-self.yeardec[0])*xpfac
        plt.subplot(2,1,1)
        plt.plot(self.yeardec,self.rhopa[:,0]*3600.,"o",color='red')
        plt.plot(self.yeardec,self.rhopa_pm[:,0]*3600.,color='blue',linestyle='dashed')
        plt.xlim(xmin,xmax)
        plt.subplot(2,1,2)
        plt.plot(self.yeardec[1:],self.rhopa[1:,1],"o",color='red')
        plt.plot(self.yeardec[1:],self.rhopa_pm[1:,1],color='blue',linestyle='dashed')
        plt.xlim(xmin,xmax)

    def plot_position(self):
        #
        av = np.zeros(self.nep); av_pm = np.zeros(self.nep)
        dv = np.zeros(self.nep); dv_pm = np.zeros(self.nep)
        eav = np.zeros(self.nep); eav_pm = np.zeros(self.nep)
        edv = np.zeros(self.nep); edv_pm = np.zeros(self.nep)
        for i in xrange(self.nep):
            av_pm[i] = self.ad_pm[i].ra.value
            dv_pm[i] = self.ad_pm[i].dec.value
            eav_pm[i] = self.err_pm[i][0]/1000./3600.
            edv_pm[i] = self.err_pm[i][1]/1000./3600.
            av[i] = self.ad[i].ra.value
            dv[i] = self.ad[i].dec.value
            eav[i] = self.err[i][0]/1000./3600.
            edv[i] = self.err[i][1]/1000./3600.
            
        plt.plot(av_pm,dv_pm,linestyle='dotted',color='blue')
        plt.errorbar(av_pm,dv_pm,xerr=eav_pm,yerr=edv_pm,color='blue',fmt='.')
        plt.plot(av,dv,'o',color='red')
        plt.errorbar(av,dv,xerr=eav,yerr=edv,color='red',fmt='o')







