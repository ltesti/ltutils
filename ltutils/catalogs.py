
import numpy as np
import astropy.io.ascii as aio
# from astroquery.simbad import Simbad
from astropy import coordinates
import astropy.units as u
try:
    from astroquery.simbad import Simbad
    have_aq = True
except:
    print('Warning: cannot load astroquery')
    have_aq = False


class SourceList(object):

    def __init__(self, infile):
        #
        self.input_file = infile
        self.intab = aio.read(self.input_file,format='csv')
        self.ra = np.zeros(len(self.intab))
        self.dec = np.zeros(len(self.intab))
        self.__get_radec()

    def __str__(self):
        rep = "Source List, read from file %s containing %d objects" % (self.input_file,len(self.ra))
        return rep

    def __get_radec(self):
        #
        for i in xrange(len(self.intab)):
            self.ra[i] = 15.*(float(self.intab['Name'][i][1:3])+(float(self.intab['Name'][i][3:5])+float(self.intab['Name'][i][5:9])/100./60.)/60.)
            self.dec[i] = float(self.intab['Name'][i][9:10]+'1.')*(float(self.intab['Name'][i][10:12])+(float(self.intab['Name'][i][12:14])+float(self.intab['Name'][i][14:17])/10./60.)/60.)

    def get_simbad_coo(self):
    	#
        if has_aq:
    	    result_table = Simbad.query_object("2MASS "+self.intab['Name'])
    	    self.coo = coordinates.SkyCoord(result_table['RA'][0]+' '+result_table['DEC'][0],unit=(u.hourangle, u.deg))
        else:
            print('cannot set self.coo, please install astroquery')
