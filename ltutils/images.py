import numpy as np
import os
import matplotlib.pyplot as plt
import astropy.io.fits as fits
from astropy.wcs import WCS
from astropy import constants as const
from astropy.convolution import Gaussian2DKernel
from astropy.convolution import convolve
from astropy import units as u
from scipy.interpolate import interp1d


class ALMAImage(object):
    
    def __init__(self, infile, subsec=False, xmin=0, xmax=-1, ymin=0, ymax=-1):
        #
        self.input_file = infile
        #
        self.subsec = subsec
        if self.subsec:
            self.xmin = int(xmin)
            self.xmax = int(xmax)
            self.ymin = int(ymin)
            self.ymax = int(ymax)
        else:
            self.xmin = 0
            self.xmax = -1
            self.ymin = 0
            self.ymax = -1

        #
        self.read_image_header()
        self.read_image_data()
        #
        self.has_discpar = False

    def __str__(self):
        #
        rep = "ALMA image for source %s, read from %s" % (self.object,self.input_file)
        #
        return rep

    def read_image_data(self):
        hdu_list = fits.open(self.input_file)
        #
        #self.naxis = hdu_list[0].header['NAXIS']
        if self.naxis == 4:
            self.image = hdu_list[0].data[0,0,self.ymin:self.ymax,self.xmin:self.xmax]
        elif self.naxis == 2:
            self.image = hdu_list[0].data[self.ymin:self.ymax,self.xmin:self.xmax]
        #
        hdu_list.close()

    def read_image_header(self):
        hdu_list = fits.open(self.input_file)
        self.header = hdu_list[0].header
        #
        self.naxis = hdu_list[0].header['NAXIS']

        self.bmaj = hdu_list[0].header['BMAJ']
        self.bmin = hdu_list[0].header['BMIN']
        self.bpa = hdu_list[0].header['BPA']

        #
        self.object = hdu_list[0].header['OBJECT']
        self.crval1 = hdu_list[0].header['CRVAL1']
        self.cdelt1 = hdu_list[0].header['CDELT1']
        self.crpix1 = hdu_list[0].header['CRPIX1']
        self.crval2 = hdu_list[0].header['CRVAL2']
        self.cdelt2 = hdu_list[0].header['CDELT2']
        self.crpix2 = hdu_list[0].header['CRPIX2']
        #
        self.npix1 = hdu_list[0].header['NAXIS1']
        self.npix2 = hdu_list[0].header['NAXIS2']
        #
        self.bmajpix = self.bmaj/abs(self.cdelt2)
        self.bminpix = self.bmin/abs(self.cdelt2)
        self.beamarea = np.pi/4/np.log(2.)*self.bmajpix*self.bminpix
        self.avg_beam_pix_stddev = np.sqrt(self.bmajpix*self.bminpix/(8.*np.log(2.)))
        #
        if self.xmax == -1:
            self.xmax = self.npix1
        if self.ymax == -1:
            self.ymax = self.npix2
        if self.subsec:
            self.crpix1 = self.crpix1 - float(self.xmin)
            self.npix1 = int(self.xmax - self.xmin)
            self.crpix2 = self.crpix2 - float(self.ymin)
            self.npix2 = int(self.ymax - self.ymin)
            self.header['CRPIX1'] = self.crpix1
            self.header['CRPIX2'] = self.crpix2
            self.header['NAXIS1'] = self.npix1
            self.header['NAXIS2'] = self.npix2
        #
        # Beam area (in pixel^2)
        self.beam_area = np.pi*self.bmaj*self.bmin/np.abs(self.cdelt1)/np.abs(self.cdelt2)/(4.*np.log(2.))
        #
        # Calc ra and dec of pixels
        self.decpix = self.crval2 + self.cdelt2*(np.linspace(1,self.npix2,self.npix2) - self.crpix2)
        self.rapix = self.crval1 - self.cdelt1*(np.linspace(1,self.npix1,self.npix1) - self.crpix1)/np.cos(self.crval2/180.*np.pi)
        
        hdu_list.close()

    def print_beam(self):
        #
        bmaj_arcsec = self.bmaj*3600.
        bmin_arcsec = self.bmin*3600.
        print('Synthesized beam for {0}: {1:.2f}x{2:.2f}, pa={3:.2f}'.format(self.input_file,bmaj_arcsec,bmin_arcsec,self.bpa))

    def rd2xy(self,ra,dec):
        #
        y = self.crpix2 + (dec - self.crval2)/self.cdelt2
        x = self.crpix1 - (self.crval1 - ra)*np.cos(self.crval2/180.*np.pi)/self.cdelt1

        inim = True
        if ((y < 0) or (x < 0) or (y>self.npix2) or (x>self.npix1)):
            inim = False
        #
        return (x, y, inim)

    def rot_ima(self):
        #
        xc = np.linspace(1,self.npix1,self.npix1) - float(self.center[0])
        yc = np.linspace(1,self.npix2,self.npix2) - float(self.center[1])
        x_rot = np.zeros(shape=(self.npix2,self.npix1))
        y_rot = np.zeros(shape=(self.npix2,self.npix1))
        #
        cost = np.cos((self.posang-90.)/180.*np.pi)
        sint = np.sin((self.posang-90.)/180.*np.pi)
        for i in range(self.npix1):
            for j in range(self.npix2):
                x_rot[j,i] = xc[i]*cost + yc[j]*sint
                y_rot[j,i] = yc[j]*cost - xc[i]*sint
        #
        return x_rot,y_rot

    def rad_prof(self,ra,dec,pa,tol):
        #
        pixtol = np.abs(tol) / np.abs(self.cdelt2)
        #
        self.ra_center = ra
        self.dec_center = dec
        self.center = self.rd2xy(ra,dec)
        self.posang = pa
        #
        radius=[]
        intensity=[]
        #
        x_rot,y_rot=self.rot_ima()
        for i in range(self.npix2):
            for j in range(self.npix1):
                if np.abs(y_rot[i,j]) <= pixtol:
                    radius.append(x_rot[i,j]*self.cdelt2)
                    intensity.append(self.image[i,j])
        #
        rv=np.array(radius)
        iv=np.array(intensity)
        #
        return rv, iv

    def get_rcell(self):
        # rotate image
        x_rot,y_rot=self.rot_ima()
        #
        # converto to angular distance
        x_rot=x_rot*self.cdelt1
        y_rot=y_rot*self.cdelt2
        #
        # deproject y_rot axis
        y_rot_dep=y_rot/np.cos(self.inc_rad)
        #
        # compute deprojected radius of each cell
        return np.sqrt(x_rot*x_rot+y_rot_dep*y_rot_dep)

    def anular_profile(self,ra,dec,pa,inc,rmax):
        #
        # Find (x,y) for rotation centre
        self.ra_center = ra
        self.dec_center = dec
        self.posang = pa
        self.pa_rad = pa/180.*np.pi
        self.inclination = inc
        self.inc_rad = self.inclination/180.*np.pi
        self.center = self.rd2xy(ra,dec)
        #
        # This is still here, because I have not tested the new version of 
        #    the script. I think it can be removed.
        #
        ## rotate image
        #x_rot,y_rot=self.rot_ima(center,pa)
        ##
        ## converto to angular distance
        #x_rot=x_rot*self.cdelt1
        #y_rot=y_rot*self.cdelt2
        ##
        ## deproject y_rot axis
        #y_rot_dep=y_rot/np.cos(inc/180.*np.pi)
        ##
        ## compute deprojected radius of each cell
        #r_cell=np.sqrt(x_rot*x_rot+y_rot_dep*y_rot_dep)
        r_cell = self.get_rcell()
        nin=np.where(r_cell <= rmax)
        print(nin)
        #
        return r_cell[nin],self.image[nin[1],nin[0]]

    def binned_annular_profile(self,ra,dec,pa,inc,rmax,dr,rmin=0.0):
        #
        # get deprojected r,i profile
        rv,iv = self.anular_profile(ra,dec,pa,inc,rmax)
        #
        # initialize radius and intensity lists
        radius=[]
        intensity=[]
        error=[]
        #
        # Start populating the radii
        dr2=dr/2.0
        if (rmin == 0.0): rmin=dr2
        rd=0.0
        ir=0
        while (rd <= rmax):
            radius.append(rmin+float(ir)*dr)
            nj = np.where( (rv > radius[ir]-dr2) & (rv <= radius[ir]+dr2))
            nn = float(len(nj[0]))
            #print("ir={0} nn={1} len(nj)={2} nj={3}".format(ir,nn,len(nj[0]),nj))
            if nn > 0.:
                intensity.append((iv[nj]).sum()/nn)
                error.append(np.sqrt(((intensity[ir]-iv[nj])**2.).sum()/nn))
            else:
                intensity.append(0.0)
                error.append(0.0)
            rd=radius[ir]
            ir += 1
        #
        # Convert to np array and return
        rar = np.array(radius)
        iar = np.array(intensity)
        ear = np.array(error)
        return rar, iar, ear

    # computes offset map for the spatial pixels
    def off_map(self):
        #
        self.center = self.rd2xy(self.ra_center,self.dec_center)
        self.xo = np.linspace(1,self.npix1,self.npix1) - float(self.center[0])
        self.yo = np.linspace(1,self.npix2,self.npix2) - float(self.center[1])
        # 
        self.has_off_map = True

    # computes map of angles from the redshifted part of the line of nodes
    def phi_map(self):
        #
        self.phi = np.zeros((self.npix2,self.npix1))
        #
        for i in range(self.npix2):
            for j in range(self.npix1):
                self.phi[i,j] = np.arctan2(self.yo[i],-1.*self.xo[j])
        #
        self.phi = self.phi + (self.pa_rad + np.pi/2.)
        #
        self.has_phi_map = True

    # 
    # this is used to set the disk parameters (used, for example for deprojecting visibilities 
    #   and for the Keplerian pattern)
    def set_discpar(self,name,ra,dec,pa,inc,ms,d):
        #
        self.discparname = name
        self.ra_center = ra
        self.dec_center = dec
        self.posang = pa
        self.pa_rad = pa/180.*np.pi
        self.inclination = inc
        self.inc_rad = self.inclination/180.*np.pi
        self.mstar = ms
        self.dist = d
        #
        self.has_discpar = True
        #
        self.off_map()
        self.phi_map()
        self.rcell = self.get_rcell()
        self.rcell_au = self.rcell*3600.*self.dist

    def print_discpar(self):
        #
        print("Using disk parameters for {0}:\n  ra={1} dec={2}\n  pa={3:7.2f}deg inc={4:5.2f}deg\n  M={5:5.2f}Msun d={6:6.2f}pc".format(self.discparname,self.ra_center,self.dec_center,self.posang,self.inclination,self.mstar,self.dist))

    def write_image_header(self,header):
        #
        header['NAXIS1'] = self.npix1
        header['CRPIX1'] = self.crpix1 
        header['NAXIS2'] = self.npix2
        header['CRPIX2'] = self.crpix2 

    # This function writes a fits file with the deprojected cube
    def write_image(self, myimage, outfile):
        #
        os.system('cp '+self.input_file+' '+outfile)
        #
        hdu_list = fits.open(outfile, mode='update')
        #
        #if self.subsec:
        if self.naxis == 4:
                npix4 = hdu_list[0].header['NAXIS4']
                newcube = np.zeros((npix4,1,self.npix2,self.npix1))
                newcube[0,0,:,:] = myimage
                if npix4 > 1:
                    newcube[1:npix4,0,:,:] = hdu_list[0].data[1:npix4,0,self.ymin:self.ymax,self.xmin:self.xmax]
                hdu_list[0].data = newcube
        elif self.naxis == 3:
                newcube = np.zeros((1,self.npix2,self.npix1))
                newcube[0,:,:] = myimage
                if npix3 > 1:
                    newcube[1:npix3,:,:] = hdu_list[0].data[1:npix3,self.ymin:self.ymax,self.xmin:self.xmax]
                hdu_list[0].data = newcube
        elif self.naxis == 2:
                hdu_list[0].data = newcube
        # else:
        #     if self.naxis == 4:
        #         newcube = np.zeros((npix4,1,self.npix2,self.npix1))
        #         hdu_list[0].data[0,0,self.ymin:self.ymax,self.xmin:self.xmax] = mycube
        #         if npix4 > 1:
        #             newcube[1:npix4,0,:,:] = hdu_list[0].data[1:npix4,0,self.ymin:self.ymax,self.xmin:self.xmax]
        #     elif self.naxis == 3:
        #         hdu_list[0].data[0,self.ymin:self.ymax,self.xmin:self.xmax] = mycube
        # 
        # Adjust header parameters as needed
        self.write_image_header(hdu_list[0].header)
        self.write_cube_header(hdu_list[0].header)
        #
        hdu_list.flush()
        hdu_list.close()

    def calc_sigma(self, img, iterations=2, rejecton_threshold=3):
        #
        # Note: I decided *not* to compute the image median (TBC)
        # my_med = np.nanmed(img)
        my_med = 0.0
        my_rms = np.nanstd(img)
        for i in range(iterations):
            delta_int = rejecton_threshold*my_rms
            wg = np.where( (img >= my_med-delta_int) & (img <= my_med+delta_int))
            my_rms = np.nanstd(img[wg])
        #
        return my_rms

    #
    # attempt to plot image
    def plot_image(self,vmin=None,vmax=None, colormap='rainbow', title=None, do_wedge=True):
        #
        if not vmin:
            vmin = np.min(self.image)
        if not vmax:
            vmax = np.max(self.image)
        #
        wcs = WCS(self.header)
        #
        #plt.subplot(projection=wcs, slices=('x', 'y', 0, 0))
        #plt.imshow(self.image, vmin=vmin, vmax=vmax, origin='lower')
        #plt.xlabel('Right Ascension J2000')
        #plt.ylabel('Declination J2000')
        ax = plt.subplot(projection=wcs, slices=('x', 'y', 0, 0))
        ax.coords['ra'].set_axislabel('Right Ascension')
        ax.coords['ra'].set_major_formatter('hh:mm:ss.s')
        ax.coords['ra'].set_format_unit(u.deg)
        ax.coords['dec'].set_axislabel('Declination')
        #ax.coords['dec'].set_coord_type('longitude')
        ax.coords['dec'].set_format_unit(u.deg)
        if not (not title):
            plt.title = title
        #if do_wedge:
        #    plt.colorbar(ax)
        ax.imshow(self.image, vmin=vmin, vmax=vmax, cmap=colormap, origin='lower')


class ALMACube(ALMAImage):
    # Modifies the class ALMAImage to read datacubes
    
    def __init__(self, infile, subsec=False, xmin=0, xmax=-1, ymin=0, ymax=-1, zmin=0, zmax=-1):
        #
        #super(ALMACube, self).__init__(infile)
        self.input_file = infile
        #
        self.subsec = subsec
        if self.subsec:
            self.xmin = xmin
            self.xmax = xmax
            self.ymin = ymin
            self.ymax = ymax
            self.zmin = zmin
            self.zmax = zmax
        else:
            self.xmin = 0
            self.xmax = -1
            self.ymin = 0
            self.ymax = -1
            self.zmin = 0
            self.zmax = -1
        #
        self.read_image_header()
        self.read_cube_additional_axis()
        self.read_cube_data()
        #
        self.verbose = True
        #
        self.has_discpar = False

    def __str__(self):
        #
        #v1 = 1.e-3*((1-self.crpix3)*self.cdelt3+self.crval3)
        #v2 = 1.e-3*((self.npix3-self.crpix3)*self.cdelt3+self.crval3)
        rep = "ALMA cube for source {0}, read from {1},\n    number of planes {2}, velocity range: from {3:7.3f}km/s to {4:7.3f}km/s".format(self.object,self.input_file,self.npix3,self.xspec[0],self.xspec[-1])

        return rep
    
    # Reads header for the third axis
    def read_cube_additional_axis(self):
        hdu_list = fits.open(self.input_file)
        #
        self.ctype3 = hdu_list[0].header['CTYPE3']
        self.crval3 = hdu_list[0].header['CRVAL3']
        self.cdelt3 = hdu_list[0].header['CDELT3']
        self.crpix3 = hdu_list[0].header['CRPIX3']
        self.npix3 = hdu_list[0].header['NAXIS3']
        self.restfqr = hdu_list[0].header['RESTFRQ']
        #
        if self.ctype3 == 'FREQ':
            self.cdelt3 = -const.c.value*self.cdelt3/self.crval3
            self.crval3 = const.c.value*(self.restfqr-self.crval3)/self.restfqr
        #
        if self.zmax == -1:
            self.zmax = self.npix3
        if self.subsec:
            self.crpix3 = self.crpix3 - self.zmin
            self.npix3 = self.zmax - self.zmin
            self.header['CRPIX3'] = self.crpix3
            self.header['NAXIS3'] = self.npix3
        #
        self.xspec = 1.e-3*((np.linspace(1,self.npix3,self.npix3)-self.crpix3)*self.cdelt3+self.crval3)
        self.nxsort = np.argsort(self.xspec)
        #
        hdu_list.close()

    # reads the data cube
    def read_cube_data(self):
        hdu_list = fits.open(self.input_file)
        #
        #self.naxis = hdu_list[0].header['NAXIS']
        if self.naxis == 4:
            self.cube = hdu_list[0].data[0,self.zmin:self.zmax,self.ymin:self.ymax,self.xmin:self.xmax]
        elif self.naxis == 3:
            self.cube = hdu_list[0].data[self.zmin:self.zmax,self.ymin:self.ymax,self.xmin:self.xmax]
        #
        hdu_list.close()

    def calc_voff_map(self, beamconvolve=False, my2Dsigma=None):
        #
        if self.has_discpar:
            if self.verbose: 
                self.print_discpar()
            self.velok_map = self.velok(self.rcell_au)
            self.vlsr_map_pixcen = self.velok_map * np.cos(self.phi) * np.sin(self.inc_rad)
            if beamconvolve:
                if not my2Dsigma:
                    my2Dsigma = self.avg_beam_pix_stddev
                kernel = Gaussian2DKernel(stddev=my2Dsigma)
                self.vlsr_map_bconv = convolve(self.vlsr_map_pixcen, kernel)
                self.vlsr_map = self.vlsr_map_bconv
            else:
                self.vlsr_map = self.vlsr_map_pixcen
            self.dpix_map = -1.e3*self.vlsr_map/self.cdelt3
        else:
            print("Error: disc parameters not set, please use the set_discpar() method to set")

    #
    def get_vkshift_disc_spec(self, rmax_au, mycube=False, cube=None):
        nin = np.where(self.rcell_au <= rmax_au)
        myspec = np.zeros(self.npix3)
        if mycube:
            ar = cube
        else:
            ar = self.cube_intoff
        myspec = np.nansum(ar[:,nin[1],nin[0]], axis=1)
        #for i in range(self.npix3):
            #ar = self.cube_intoff[i,:,:]
            #myspec[i] = ar[nin[1],nin[0]].sum()
        area = float(len(nin[0]))
        return self.xspec, myspec, area

    def do_vkshift_spec(self,rmax_au,voff): #name,ra,dec,pa,inc,ms,d,rmax_au,voff):
        #
        #
        vv,ff = self.get_vkshift_disc_spec(rmax_au)
        return vv-voff,ff

    def do_vkshift_intima(self,vmin,vmax,voff): #name,ra,dec,pa,inc,ms,d,vmin,vmax,voff):
        #
        #
        nv = np.where((self.xspec >= vmin+voff) & (self.xspec <= vmax+voff))
        self.intima = np.sum(self.cube_intoff[nv],axis=0)

        # This function writes a fits file with the deprojected cube
    def write_image_plane(self, myimage, outfile):
        #
        os.system('cp '+self.input_file+' '+outfile)
        #
        hdu_list = fits.open(outfile, mode='update')
        #
        #if self.subsec:
        if self.naxis == 4:
            hdu_list[0].header['NAXIS4'] = 1
            hdu_list[0].header['NAXIS3'] = 1
            newcube = np.zeros((1,1,self.npix2,self.npix1))
            newcube[0,0,:,:] = myimage
            hdu_list[0].data = newcube
        elif self.naxis == 3:
            hdu_list[0].header['NAXIS3'] = 1
            newcube = np.zeros((1,self.npix2,self.npix1))
            newcube[0,:,:] = myimage
            hdu_list[0].data = newcube
        elif self.naxis == 2:
            hdu_list[0].data = newcube
        # Adjust header parameters as needed
        self.write_image_header(hdu_list[0].header)
        self.write_cube_header(hdu_list[0].header)
        #
        hdu_list.flush()
        hdu_list.close()

    def set_deprojectkep(self,name,ra,dec,pa,inc,ms,d,beamconvolve=False):
        #
        self.set_discpar(name,ra,dec,pa,inc,ms,d)
        self.calc_voff_map(beamconvolve=beamconvolve)

    def make_vkshift_cube(self,interpol='linear'):
        #
        self.cube_intoff = np.zeros((self.npix3,self.npix2,self.npix1))
        for ix in range(self.npix1):
            for iy in range(self.npix2):
                #
                # This option shifts the spectra by integer pixels
                if interpol == 'pixshift':
                    pmin = int(np.round(self.dpix_map[iy,ix]))
                    pmax = int(self.npix3+pmin)
                    #print("pmin={0} pmax={1} npix3={2}".format(pmin,pmax,self.npix3))
                    #print("iy={0} ix={1}".format(iy,ix))
                    #print("self.cube_intoff[0:{0},{4},{3}] = self.cube[{1}:{2},{4},{3}]".format(pmax,int(-pmin),self.npix3,ix,iy))
                    #print("shape(cube)={0} {1}".format(np.shape(self.cube),np.shape(self.cube_intoff)))
                    if pmin < 0:
                        self.cube_intoff[0:pmax,iy,ix] = self.cube[-pmin:self.npix3,iy,ix]
                    if pmax >= self.npix3:
                        self.cube_intoff[pmin:self.npix3,iy,ix] = self.cube[0:self.npix3-pmin,iy,ix]
                #
                # This option interpolates the spectra at the computed velocity 
                #  either using the linear or cubic spline interpolation
                elif (interpol == 'linear' or interpol == 'cubic'):
                    xshift = self.xspec - self.vlsr_map[iy,ix]
                    spinter = interp1d(xshift, self.cube[:,iy,ix], kind=interpol, bounds_error=False, fill_value=0.0)
                    self.cube_intoff[:,iy,ix] = spinter(self.xspec)
                else:
                    print("Error: interpol keywork must be one of 'pixshift', 'linear' or 'cubic' (entered: {0})".format(interpol))

    #
    # Extract spectrum at given location
    def get_spec_pix(self,x,y,intoff=False):
        #
        x=x-self.xmin
        y=y-self.ymin
        if intoff:
            myspec = self.cube_intoff[:,y-1,x-1]
        else:
            myspec = self.cube[:,y-1,x-1]
        return self.xspec, myspec

    #
    # extract spectrum within elliptical area, uses the disk projection
    def get_spec_ellipse(self,myimage,name,ra,dec,pa,inc,ms,d,rmax, rmin=None, force_discpar=False):
        #
        # ToDo: instead of this, use the has_discpar attribute to ask to run self.set_discpar
        #       I also think that it would be useful to modify the headerfor dekep fits.
        if force_discpar or not self.has_discpar:
            self.set_discpar(name,ra,dec,pa,inc,ms,d)
        #
        x, myspec, areapix = self.get_vkshift_disc_spec(rmax, mycube=True, cube=myimage)
        if not rmin or rmin > rmax:
            myspec = myspec * areapix/self.beam_area
        else:
            #ninner=np.where(r_cell <= rint)
            x, myinspec, inareapix = self.get_vkshift_disc_spec(rmin, mycube=True, cube=myimage)
            myspec = (myspec - myinspec)*(areapix-inareapix)/self.beam_area

        return self.xspec, myspec

    #
    # Computes the indices to sum through for a given velocity range
    def _get_irange_spec(self,vmin,vmax):
        #
        #
        if vmin > vmax:
             tmp_v = vmax
             vmax = vmin
             vmin = tmp_v
        if vmin >= self.xspec[self.nxsort[-1]]:
            imin = len(self.nxsort) - 1
            imax = len(self.nxsort)
        elif vmax <= self.xspec[self.nxsort[0]]:
            imin = 0
            imax = 1
        else:
            if vmin <= self.xspec[self.nxsort[0]]:
                imin = 0
            else:
                imin = np.argmin(self.xspec[self.nxsort] < vmin)-1
            if vmax >= self.xspec[self.nxsort[-1]]:
                imax = len(self.nxsort)
            else:
                imax = np.argmax(self.xspec[self.nxsort] > vmax)+1
        #
        nmin = self.nxsort[imin]
        nmax = self.nxsort[imax-1]
        if nmin > nmax:
            return nmax, nmin+1
        else:
            return nmin, nmax+1

    #
    # Function to construct the integration range map
    #   requires in input the imin, imax as a function of radius
    #   creates the matrices of m0_imin and m0_imax for the integration
    def _create_integration_pixap(self,r,iminmax):
        #
        self.m0_imin = (np.copy(self.rcell_au)*0)
        self.m0_imax = (np.copy(self.rcell_au)*0)
        # First we fill the inner range
        nin = np.where(self.rcell_au < r[1])
        self.m0_imin[nin] = iminmax[0][0]
        self.m0_imax[nin] = iminmax[0][1]
        # then we fill the intermediate
        for i in range(len(r)-2):
            nin = np.where((self.rcell_au >= r[i+1]) & (self.rcell_au < r[i+2]))
            self.m0_imin[nin] = iminmax[i+1][0]
            self.m0_imax[nin] = iminmax[i+1][1]
        nin = np.where(self.rcell_au >= r[-1])
        self.m0_imin[nin] = iminmax[-1][0]
        self.m0_imax[nin] = iminmax[-1][1]

    #
    # Compute integrated image using the m0_imin and m0_imax arrays
    #   multiply properly for channel width, optionally write output fits
    def compute_m0_map(self, outfile=None):
        #
        #for z in range(len(self.m0_imin)):
        self.m0 = 0.*np.copy(self.m0_imin)
        for j in range(self.npix2):
            for i in range(self.npix1):
                self.m0[j,i] = np.sum(self.cube[int(self.m0_imin[j,i]):int(self.m0_imax[j,i]),j,i])
        self.m0 = self.m0 * (self.xspec[1]-self.xspec[0])
        # if outfile is defined, writes the output fits
        if outfile:
            self.write_image_plane(self.m0, outfile)   

    #
    # computes the keplerian velocity for an array of radii
    # ms is in Msun, r is in AU, returns velocity in km/s
    def velok(self,r):
        #
        # Keplerian velocity
        # konst = sqrt(1e-8*1.e33/1e13)/1e5 = sqrt(1e12)/1e5 = 10.
        g = 6.6742  # e-8
        msun = 1.989  #e33 # g
        au = 1.496  #e13 # cm
        konst = 10.
        #
        mg = self.mstar*msun
        rcm = r*au
        #
        v = np.sqrt(g*mg/rcm) * konst  #/1.e5
        #
        return v

    # This function writes a fits file with the deprojected cube
    def write_cube(self, mycube, outfile):
        #
        os.system('cp '+self.input_file+' '+outfile)
        #
        hdu_list = fits.open(outfile, mode='update')
        #
        #if self.subsec:
        if self.naxis == 4:
                npix4 = hdu_list[0].header['NAXIS4']
                newcube = np.zeros((npix4,self.npix3,self.npix2,self.npix1))
                newcube[0,:,:,:] = mycube
                if npix4 > 1:
                    newcube[1:npix4,:,:,:] = hdu_list[0].data[1:npix4,self.zmin:self.zmax,self.ymin:self.ymax,self.xmin:self.xmax]
                hdu_list[0].data = newcube
        elif self.naxis == 3:
                hdu_list[0].data = mycube
        # 
        # Adjust header parameters as needed
        self.write_image_header(hdu_list[0].header)
        self.write_cube_header(hdu_list[0].header)
        #
        hdu_list.flush()
        hdu_list.close()

    def write_cube_header(self,header):
        #
        header['NAXIS3'] = self.npix3
        header['CRPIX3'] = self.crpix3 
        header['CRVAL3'] = self.crval3
        header['CDELT3'] = self.cdelt3
        header['CTYPE3'] = 'VRAD'
        header['CUNIT3'] = 'm/s'




