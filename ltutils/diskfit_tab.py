__author__ = 'ltesti - 12 Jul 2016'

import numpy as np
import astropy.io.ascii as aio
from astropy.table import Column
import astropy.units as u
from astropy.analytic_functions import blackbody_nu


class FitResult(object):
    
    def __init__(self, stardat, debug=False):
        #
        # red the table file
        self.m_earth = 5.9722e27
        self.verbose = debug
        self.fitfile = stardat['fit_file']
        self.ID = stardat['ID']
        self.name = stardat['Name']
        self.dist = stardat['Dist']
        self.teff = stardat['Teff']
        self.lstar = stardat['Lstar']
        self.mstar = stardat['Mstar']
        self.iflar = stardat['iflar']
        self.valid = stardat['Valid']
        self.fcont = stardat['F_cont']
        self.econt = stardat['E_cont']
        self.a = stardat['a']
        self.ea = stardat['e_a']
        self.m20k = self.mdust(use20k=True,k340=2.0)
        self.fit = aio.read(self.fitfile)
        #
        mypar = {'starname' : self.get_name,
                 'par0' : self.get_gamma,
                 'par1' : self.get_sig0,
                 'par2' : self.get_rc,
                 'par3' : self.get_inc,
                 'r_out' : self.get_rout,
                 'm_dust' : self.get_mdust,
                 'flux_int' : self.get_fcum,
                 'temp_mid' : self.get_tmid
                 }
        for ir in self.fit['field']:
            try:
                mypar[ir]()
            except:
                if self.verbose:
                    print('ID={0}, do not know how to handle parameter {1}'.format(self.ID,ir))
        #
        self.lab_par = {'ID' : self.ID,
                        'Name' : self.name,
                        'Dist' : self.dist,
                        'Teff' : self.teff,
                        'Lstar' : self.lstar,
                        'Mstar' : self.mstar,
                        'iflar' : self.iflar,
                        'Valid' : self.valid,
                        'F_cont' : self.fcont,
                        'E_cont' : self.econt,
                        'a' : self.a,
                        'e_a' : self.ea,
                        'Mdust' : self.m20k,
                        'M_dust' : self.m,
                        'R_out' : self.ro,
                        'F_cumul' : self.fcum,
                        'gamma' : self.g,
                        'Rc' : self.rc,
                        'sigma0' : self.sig0,
                        'inc' : self.inc,
                        'T_mid' : self.tmid
                         }
    
    def __str__(self):
        string = 'Fit for {0}: read {1} lines from {2}'.format(self.star_name,len(self.fit),self.fitfile)
        return string

    def get_name(self):
        self.star_name = self.fit[0]['info']
        
    def get_gamma(self):
        self.g = self.get_par(1)
        
    def get_sig0(self):
        self.sig0 = self.get_par(2)
        
    def get_rc(self):
        self.rc = self.get_par(3)
        
    def get_inc(self):
        self.inc = self.get_par(4)
        
    def get_rout(self):
        self.ro = self.get_par(8)
        
    def get_mdust(self):
        self.m = self.get_par(9)
        self.m[0] = 'M$_{\rm Earth}$'
        for i in [1,2,3,4]:
            self.m[i] = self.m[i]/self.m_earth

    def get_fcum(self):
        self.fcum = self.get_par(10)
        
    def get_tmid(self):
        self.tmid = self.get_par(11)
        
    def get_par(self, ip):
        return [self.fit[ip]['info'],self.fit[ip]['bestfit'],self.fit[ip]['16th'],self.fit[ip]['50th'],self.fit[ip]['84th']]
        
    def string_fit_line(self):
        #to_print = [ self.ro, self.fcum, self.m, self.g, self.rc, self.sig0, self.inc, self.tmid ] 
        to_print = [ 'ID', 'R_out', 'F_cumul', 'M_dust', 'gamma', 'Rc', 'sigma0', 'inc', 'T_mid' ] 
        string = ''
        for myp in to_print:
            mypar = self.get_par_lab(myp)
            if ((type(mypar) == str) or (type(mypar) == np.string_)):
                string = string+'{:13s}'.format(mypar)
            elif (type(mypar) == int or type(mypar) == float):
                string = string+'{:013.9f} '.format(mypar)
            elif len(mypar) >= 5:
                string = string+'{:013.9f} {:013.9f} {:013.9f} '.format(mypar[2],mypar[3],mypar[4])
            else:
                string = string+'Panico! type(mypar)={0} '.format(type(mypar))
        return string
    
    def get_par_lab(self,label):
        try:
            return self.lab_par[label]
        except:
            print ('Label {0} not found, returning ID'.format(label))
            return self.lab_par('ID')
    
    def mdust(self, use20k=True, k340=3.37):
        if use20k:
            myt = 20.
        else:
            # This is Andrews 2013 prescription
            myt = 25. * allstars['Lstar'][i]**0.25
        return 1.5943e-17 * (self.fcont * (self.dist)**2) / (blackbody_nu(340. * u.GHz, myt * u.K).value * k340)

        

#
# This is the main class that reads the table describing thestars properties
# and the fits results files and offers methods to collate the result in a 
# final table and write it on disk
class FitsSet(object):
    
    def __init__(self, intable):
        #
        #
        self.infile = intable
        self.allfits = aio.read(self.infile)
        self.allrows = [FitResult(self.allfits[i]) for i in range(len(self.allfits))]
        
    def __str__(self):
        myrepr = 'Tabella dei risultati dei fit: lette {0} righe dal file {1}'.format(len(self.allrows),self.infile)
        return myrepr
    
    def create_full_table(self):
        self.full_tab = self.allfits
        
    def fill_full_table(self):
        self.create_full_table()
        labels = ['M_dust','R_out','F_cumul','gamma','Rc','sigma0','inc','T_mid']
        for lab in labels:
            self.add_column_label(lab)
        self.correct_mdust_column()
        
    def correct_mdust_column(self):
        for i in range(len(self.allfits)):
            self.full_tab[i]['M_dust'] = self.allrows[i].m20k
        
    def add_column_label(self, lab):
        c1 = []
        c2 = []
        c3 = []
        for i in range(len(self.allfits)):
            myvec = self.allrows[i].get_par_lab(lab)
            c1.append(myvec[2])
            c2.append(myvec[3])
            c3.append(myvec[4])
        self.full_tab[lab+'_16'] = Column(c1)
        self.full_tab[lab+'_50'] = Column(c2)
        self.full_tab[lab+'_84'] = Column(c3)

    def write_table_ipac(self, outname):
        aio.write(self.full_tab,outname,format='ipac')
