Author:

- `Leonardo Testi (ESO, Garching)`_

Direct contributions to the code:

- `Marco Tazzari (IoA, Cambridge) <https://github.com/mtazzari>`_
- `Greta Guidi (INAF Arcetri, Florence) 