from __future__ import print_function
import numpy as np
import os
import astropy.io.fits as fits
from astropy import constants as const
from scipy.interpolate import interp1d
from .images import ALMAImage, ALMACube

def select_parameters(key, params, defaults, no_default):
    try:
        return params[key]
    except KeyError:
        if no_default:
            raise ValueError("Cannot initiate NACOCube with default values ({})\n {}".format(key,params))
        else:
            return defaults[key]


class NACOCube(ALMACube):
    # Modifies the class ALMACube to read and work with NACO datacubes

    default_naco_params = {'infile': None,   # input file, no default
                           'subsec': False,  # read subsection
                           'xmin': 0,  # subsection parameters
                           'xmax': -1,
                           'ymin': 0,
                           'ymax': -1,
                           'zmin': 0,
                           'zmax': -1,
                           'domask': True,  # Mask outer region in AGPM
                           'xv': 431,  # Vortex x-center
                           'yv': 392,  # Vortex y-center
                           'rv': 280,  # radius of useful field
                           'discard_first': True,  # Discard first planes of the cube
                           'n_discard_first': 1,    # Number of initial planes to discard
                           'discard_last': True,  # Discard last plane of the cube
                           'parang_mode': 'frame',  # mode used to compute parang
                           'no_default': True,
                           }
    
    def __init__(self, parameters_user):
        #
        if 'no_default' in parameters_user:
            self.no_default = parameters_user['no_default']
        else:
            self.no_default = True

        self.parameters = {key: select_parameters(key, parameters_user,
                                                  self.default_naco_params, self.no_default)
                               for key in self.default_naco_params}

        #self.parameters = self.default_naco_params

        if not self.parameters['infile']:
            raise ValueError("Cannot initiate NACOCube without input file: set \'infile\' in input dictionary")
        else:
            self.input_file = self.parameters['infile']

        # set up the region to be read from the cube
        self.discard_first_plane = self.parameters['discard_first']
        self.n_discard_first = self.parameters['n_discard_first']
        self.discard_last_plane = self.parameters['discard_last']
        self._set_img_sect()
        #
        self.read_image_header()
        self._set_cube_planes_and_xy_limits()
        self.read_cube_data()
        if self.domask:
            self._set_mask_agpm()
        #
        self.parang = self.get_parang()
        #
        self.verbose = True
        #

    def _set_cube_planes_and_xy_limits(self):
        #
        if self.subsec:
            if self.discard_first_plane:
                self.zmin = max(self.n_discard_first,self.parameters['zmin'])
            else:
                self.zmin = max(0,self.parameters['zmin'])
            if self.discard_last_plane:
                self.zmax = min(self.naxis3-1,self.parameters['zmax']+1)
            else:
                self.zmax = min(self.naxis3,self.parameters['zmax']+1)
        else:
            if self.discard_first_plane:
                self.zmin = self.n_discard_first
            else:
                self.zmin = 0
            if self.discard_last_plane:
                self.zmax = self.naxis3-1
            else:
                self.zmax = self.naxis3

        self.xmax = min(self.xmax+1,self.naxis1)
        self.ymax = min(self.ymax+1,self.naxis2)
        self.npix1 = self.xmax - self.xmin
        self.npix2 = self.ymax - self.ymin
        self.npix3 = self.zmax - self.zmin
        #

    def _set_img_sect(self):
        # Set region to be read
        self.subsec = self.parameters['subsec']
        self.domask = self.parameters['domask']

        if self.domask:
            self.vxmin = self.parameters['xv'] - self.parameters['rv']
            self.vxmax = self.parameters['xv'] + self.parameters['rv']
            self.vymin = self.parameters['yv'] - self.parameters['rv']
            self.vymax = self.parameters['yv'] + self.parameters['rv']
        else:
            self.vxmin = 0
            self.vxmax = -1
            self.vymin = 0
            self.vymax = -1

        if self.subsec:
            self.xmin = max(self.parameters['xmin'],self.vxmin)
            self.xmax = min(self.parameters['xmax'],self.vxmax)
            self.ymin = max(self.parameters['ymin'],self.vymin)
            self.ymax = min(self.parameters['ymax'],self.vymax)
        else:
            self.xmin = self.vxmin
            self.xmax = self.vxmax
            self.ymin = self.vymin
            self.ymax = self.vymax

        self.xv = self.parameters['xv'] - self.xmin
        self.yv = self.parameters['yv'] - self.ymin
        self.rv = self.parameters['rv']

    def get_parang(self):
        #
        if self.parameters['parang_mode'] == 'avg':
            p = np.ones(self.npix3) * self._get_parang({'mode': self.parameters['parang_mode']})
        elif self.parameters['parang_mode'] == 'frame':
            p = np.zeros(self.npix3)
            for i in range(len(p)):
                p[i] = self._get_parang({'mode': self.parameters['parang_mode'], 'plane': i})
        else:
            raise ValueError("parang_mode={0} not recognized in get_parang".format(self.parameters['parang_mode']))
        #
        return p

    def _get_parang(self, params={'mode': 'avg', 'plane': None}):
        #
        if params['mode'] == 'avg':
            return (self.parang_start + self.parang_end)/2.0
        elif params['mode'] == 'frame':
            if not params['plane'] and params['plane']!=0:
                raise ValueError("Mode={0} requires a valid 'plane' input in _get_parang, params = {1}".format(params['mode'],params))
            else:
                return (self.parang_start + (self.parang_end - self.parang_start)/(float(self.naxis3)-1.)*float(self.zmin+params['plane']))
        else:
            raise ValueError("Mode={0} not recognized in _get_parang".format(params['mode']))

    def _set_mask_agpm(self):
        #dstmat = np.zeros((self.npix2,self.npix1))
        x = np.linspace(1,self.npix1,self.npix1) - self.xv
        y = np.linspace(1,self.npix2,self.npix2) - self.yv
        xx = np.repeat(x[np.newaxis,:],self.npix2,axis=0)
        yy = np.repeat(y[:,np.newaxis],self.npix1,axis=1)
        dist = np.repeat((xx*xx + yy*yy)[np.newaxis,:,:],self.npix3,axis=0)
        r2 = self.rv*self.rv
        wmask = np.where(dist > r2)
        self.cube[wmask] = np.nan

    def get_cube_noise(self,cube,r1=80,dr=150):
        xc = (float(self.npix1)/2.)
        yc = (float(self.npix2)/2.)
        x = np.linspace(1,self.npix1,self.npix1) - xc
        y = np.linspace(1,self.npix2,self.npix2) - yc
        x = np.repeat(x[np.newaxis,:],self.npix2,axis=0)
        y = np.repeat(y[:,np.newaxis],self.npix1,axis=1)
        dist = np.sqrt(x*x+y*y)
        wan = np.where((dist >= r1) & (dist <=r1+dr))
        rms = []
        for i in range(self.npix3):
            rms.append(np.std(cube[i][wan]))
        return rms

    def subcube(self, bkg_ima):        
        self.sub_cube = self.cube[:] - bkg_ima
        self.has_sub_cube = True

    def read_image_header(self):
        hdu_list = fits.open(self.input_file)
        #
        self.naxis = hdu_list[0].header['NAXIS']
        self.naxis1 = hdu_list[0].header['NAXIS1']
        self.naxis2 = hdu_list[0].header['NAXIS2']
        self.naxis3 = hdu_list[0].header['NAXIS3']
        #self.npix1 = hdu_list[0].header['NAXIS1']
        #self.npix2 = hdu_list[0].header['NAXIS2']
        #self.npix3 = hdu_list[0].header['NAXIS3']

        self.exptime = hdu_list[0].header['EXPTIME']
        self.ndit = hdu_list[0].header['HIERARCH ESO DET NDIT']
        self.catg = hdu_list[0].header['HIERARCH ESO DPR CATG']
        self.type = hdu_list[0].header['HIERARCH ESO DPR TYPE']
        #
        self.object = hdu_list[0].header['ESO OBS TARG NAME']
        self.parang_start = hdu_list[0].header['ESO TEL PARANG START']
        self.parang_end = hdu_list[0].header['ESO TEL PARANG END']
        self.expno = hdu_list[0].header['ESO TPL EXPNO']
        self.nexp = hdu_list[0].header['ESO TPL NEXP']
        #
        self.crpix1 = hdu_list[0].header['CRPIX1']
        self.crpix2 = hdu_list[0].header['CRPIX2']
        self.crval1 = hdu_list[0].header['CRVAL1']
        self.crval2 = hdu_list[0].header['CRVAL2']
        self.cd1_1 = hdu_list[0].header['CD1_1']
        self.cd2_1 = hdu_list[0].header['CD2_1']
        self.cd1_2 = hdu_list[0].header['CD1_2']
        self.cd2_2 = hdu_list[0].header['CD2_2']
        self.new_plscale = np.sqrt(self.cd1_1**2+self.cd2_1**2)
        #
        #
        if self.xmax == -1:
            self.xmax = self.npix1
        if self.ymax == -1:
            self.ymax = self.npix2
        if self.subsec:
            self.crpix1 = self.crpix1 - self.xmin
            self.npix1 = self.xmax - self.xmin 
            self.crpix2 = self.crpix2 - self.ymin
            self.npix2 = self.ymax - self.ymin 
        #        
        # Calc ra and dec of pixels
        #dx = np.linspace(1,self.npix1,self.npix1) - self.crpix1
        #dy = np.linspace(1,self.npix2,self.npix2) - self.crpix2
        #self.decpix = self.crval2 + self.cd2_1 * dx + self.cd2_2 * dy
        #self.rapix = self.crval1 - (self.cd1_1 * dx + self.cd1_2 * dy)/np.cos(self.crval2/180.*np.pi)
        
        hdu_list.close()

    def __str__(self):
        #
        #v1 = 1.e-3*((1-self.crpix3)*self.cdelt3+self.crval3)
        #v2 = 1.e-3*((self.npix3-self.crpix3)*self.cdelt3+self.crval3)
        rep = "NACO cube for source {0}, read from {1},\n    number of planes {2}".format(self.object,self.input_file,self.npix3)

        return rep

    # Writes all the frames in a cube as separate sequential files and returns
    #   the number of frames written (useful for updating the counter)
    def write_all_planes(self, data, basename, nfirst):
        #
        nframe = 0
        for i in range(self.npix3):
            filename = basename+'_{0:04d}'.format(nfirst+nframe)+'.fits'
            #self.write_naco_image_plane(data[i,:,:], filename, parang=self.parang[i])
            self.write_minimal_frame(data[i,:,:], filename, parang=self.parang[i])
            nframe += 1
        return nframe
    # Writes all the frames in a cube as separate sequential files and returns
    #   the number of frames written (useful for updating the counter)
    def write_all_planes_below_rms_thres(self, data, rms, threshold, basename, nfirst):
        #
        nframe = 0
        for i in range(self.npix3):
            if rms[i] <= threshold:
                filename = basename+'_{0:04d}'.format(nfirst+nframe)+'.fits'
                #self.write_naco_image_plane(data[i,:,:], filename, parang=self.parang[i])
                self.write_minimal_frame(data[i,:,:], filename, parang=self.parang[i])
                nframe += 1
        return nframe

    def write_minimal_frame(self, myimage, outfile, parang=None):
        #
        imtowrite = np.copy(myimage)
        wnan = np.where(np.isnan(imtowrite))
        #winf = np.where(imtowrite == np.inf)
        imtowrite[wnan] = 0.0
        #imtowrite[winf] = 0.0
        hl1 = fits.HDUList()
        hd1 = fits.PrimaryHDU(imtowrite)
        hl1.append(hd1)
        hl1[0].header['NEW_PARA'] = parang
        hl1[0].header['CDELT2']=self.new_plscale
        hl1[0].header['CDELT1']=-self.new_plscale
        hl1.writeto(outfile)
    
    # Writes a 2d image with adjusted header
    def write_naco_image_plane(self, myimage, outfile, parang=None):
        #
        os.system('cp '+self.input_file+' '+outfile)
        #
        hdu_list = fits.open(outfile, mode='update')
        #
        #if self.subsec:
        if self.naxis == 4:
            del hdu_list[0].header['NAXIS4']
            del hdu_list[0].header['NAXIS3']
            newcube = np.zeros((self.npix2,self.npix1))
            newcube[:,:] = myimage
            #hdu_list[0].data = newcube
        elif self.naxis == 3:
            del hdu_list[0].header['NAXIS3']
            newcube = np.zeros((self.npix2,self.npix1))
            newcube[:,:] = myimage
            #hdu_list[0].data = newcube
        
        #elif self.naxis == 2:
        wnan = np.where(np.isnan(newcube))
        newcube[wnan] = 0.0
        hdu_list[0].data = newcube

        #
        if not parang:
            pass
        else:
            hdu_list[0].header['NEW_PARA'] = parang

        hdu_list[0].header['CDELT2']=self.new_plscale
        hdu_list[0].header['CDELT1']=-self.new_plscale


        # else:
        #     if self.naxis == 4:
        #         newcube = np.zeros((npix4,1,self.npix2,self.npix1))
        #         hdu_list[0].data[0,0,self.ymin:self.ymax,self.xmin:self.xmax] = mycube
        #         if npix4 > 1:
        #             newcube[1:npix4,0,:,:] = hdu_list[0].data[1:npix4,0,self.ymin:self.ymax,self.xmin:self.xmax]
        #     elif self.naxis == 3:
        #         hdu_list[0].data[0,self.ymin:self.ymax,self.xmin:self.xmax] = mycube
        # 
        # Adjust header parameters as needed
        self.write_image_header(hdu_list[0].header)
        self.write_naco_cube_header(hdu_list[0].header)
        #
        hdu_list.flush()
        hdu_list.close()

    # This function writes a fits file with the deprojected cube
    def write_naco_cube(self, mycube, outfile):
        #
        os.system('cp '+self.input_file+' '+outfile)
        #
        hdu_list = fits.open(outfile, mode='update')
        #
        #if self.subsec:
        if self.naxis == 4:
                npix4 = hdu_list[0].header['NAXIS4']
                newcube = np.zeros((npix4,self.npix3,self.npix2,self.npix1))
                newcube[0,:,:,:] = mycube
                if npix4 > 1:
                    newcube[1:npix4,:,:,:] = hdu_list[0].data[1:npix4,self.zmin:self.zmax,self.ymin:self.ymax,self.xmin:self.xmax]
                hdu_list[0].data = newcube
        elif self.naxis == 3:
                hdu_list[0].data = mycube
        # else:
        #     if self.naxis == 4:
        #         hdu_list[0].data[0,self.zmin:self.zmax,self.ymin:self.ymax,self.xmin:self.xmax] = mycube
        #     elif self.naxis == 3:
        #         hdu_list[0].data[self.zmin:self.zmax,self.ymin:self.ymax,self.xmin:self.xmax] = mycube
        # 
        # Adjust header parameters as needed
        self.write_image_header(hdu_list[0].header)
        self.write_naco_cube_header(hdu_list[0].header)
        #
        hdu_list.flush()
        hdu_list.close()

    def write_naco_cube_header(self,header):
        #
        header['NAXIS3'] = self.npix3
        #header['CRPIX3'] = self.crpix3 
        #header['CRVAL3'] = self.crval3
        #header['CDELT3'] = self.cdelt3
        #header['CTYPE3'] = 'VRAD'
        #header['CUNIT3'] = 'm/s'




