# import astropy
from astropy.io import fits
# import os
import numpy as np
import matplotlib.pyplot as plt
# import matplotlib.lines as lin
# from matplotlib.ticker import NullFormatter
from matplotlib import gridspec
# from matplotlib.ticker import MultipleLocator, FormatStrFormatter
# import numpy.polynomial.polynomial as polf
# from astropy.io import ascii
# from decimal import *
from astropy.coordinates import get_icrs_coordinates
from astropy.coordinates import SkyCoord
# from astropy.coordinates import ICRS
# from astropy import units as u
# from astroquery.simbad import Simbad

try:
    from astroquery.simbad import Simbad
    have_aq = True
except:
    print('Warning: cannot load astroquery')
    have_aq = False

cgsJy = 1.e-23
arcsec2rad = 1. / 206265.
fontlab = 15
fonticks = 10
markers = 8.


class CASAfitsimage(object):
    """
    Initialize object in CASA as e.g. im = CASAfitsimage('imagename.fits', ia)

    """
    def __init__(self, image, ia):
        self.imagename = image
        self.ia = ia
        self.hdu = fits.open(image)
        self.flux = np.transpose(self.hdu[0].data[0, 0, :, :])
        self.npixel1 = self.hdu[0].header['NAXIS1']
        self.npixel2 = self.hdu[0].header['NAXIS2']

    def get_beam(self):
        bmaj = self.hdu[0].header['BMAJ'] * 3600.  # arcsec
        bmin = self.hdu[0].header['BMIN'] * 3600.
        bpa = self.hdu[0].header['BPA']  # deg
        bmed = np.round(np.sqrt(bmaj * bmin), 1)

        return bmaj, bmin, bpa

    @property
    def rms(self):
        """
        Returns the rms of the image, over a region (hopefully) without the source.

        """
        return np.std(self.flux[0:self.npixel2 / 3., 0:self.npixel1 / 3.])

    def get_center(self, objectid=''):
        """
        Compute the center of the source.

        Parameters
        ----------
        objectid : str
            A valid identifier for the source to be recognized by Simbad.

        Returns
        -------
        ra0, dec0 : float
            R.A. and Dec of the source center, computed accounting for the
            proper motions from J2000 to the date of the observations
            listed in the fits header.

            If objectid = '', ra0 and dec0 are the central coordinates
            from a 2D gaussian fit of the image

        """
        if not has_aq:
            print('Cannot resolve (missing astroquery package), resetting objectid to null')
            objectid = ''
        if objectid == '':
            self.ia.open(self.imagename)
            m0 = self.ia.fitcomponents()['results']['component0']['shape'][
                'direction']['m0']['value']
            m1 = self.ia.fitcomponents()['results']['component0']['shape'][
                'direction']['m1']['value']
            c = SkyCoord(m0, m1, unit='rad')
            ra0 = c.ra.value
            dec0 = c.dec.value
            self.ia.close()

        else:
            dtime = (
                    float(self.hdu[0].header['DATE-OBS'][0:4]) - 2000.) + \
                    float(self.hdu[0].header['DATE-OBS'][5:7]) / 12.  # yr
            c = get_icrs_coordinates(objectid)
            cra = c.ra.deg
            cdec = c.dec.deg
            cosdec = np.cos(cdec * np.pi / 180.)
            custom = Simbad()
            custom.add_votable_fields('propermotions')
            pmra = custom.query_object(objectid)['PMRA'][0]  # mas/yr
            pmdec = custom.query_object(objectid)['PMDEC'][0]
            ra0 = cra + (pmra * dtime * pow(10, -6) / 3.6) / cosdec
            dec0 = cdec + (pmdec * dtime * pow(10, -6) / 3.6)
            print('centro: ' + str(ra0) + str(dec0))
            print('Proper motions:\npmra: ' + str(pmra) + ' ' + str(
                custom.query_object(objectid)['PMRA'].unit))
            print('pmdec: ' + str(pmdec) + ' ' + str(
                custom.query_object(objectid)['PMDEC'].unit))

        return ra0, dec0

    def flux_profile(self, incl, pa, dist, ra0, dec0, binau, nodi=False,
                     plotfilename=None):
        """
        Compute the radial flux profile, deprojecting for the given inc and PA.

        Parameters
        ----------
        incl : float
            Inclination (deg).
        pa : float
            Position angle (deg).
        dist : float
            Distance (parsec).
        ra0 : float
            R.A. of the source center (deg).
        dec0 : float
            Dec of the source center (deg).
        binau : array, float
            Radial bin centers where the flux density is computed (au).
            Only positive values.
        nodi : bool, optional
            If True, computes the flux along the disk major axis [-binau, +binau].
        plotfilename : str, optional
            If specified, shows a plot of the flux density and saves it to plotfilename.

        Returns
        -------
        bin : array, float
            Bin centers (au).
        fmed : array, float
            Flux profile at the bin centers (Jy/beam).
        err_fmed : array, float
            Uncertainty on each flux profile bin (Jy/beam), computed as the std
            of the averaged pixels in each bin.

        TODO: implement: to account for the non-independent pixels in one beam, set ddof=dd
            instead of ddof=1 on lines 167 and 184

        """
        # xmax = 400. #au
        xmax = max(binau)
        xmin = 0.
        if nodi:
            xmin = -xmax

        b1, b2, beampa = self.get_beam()  # arcsec
        rismin = np.around(min(b1, b2), 2)
        nu = self.hdu[0].header['RESTFRQ']  # Hz
        pa = pa * (np.pi) / 180.
        incl = incl * (np.pi) / 180.

        crpix1 = self.hdu[0].header['CRPIX1']
        crval1 = self.hdu[0].header['CRVAL1']
        crpix2 = self.hdu[0].header['CRPIX2']
        crval2 = self.hdu[0].header['CRVAL2']
        cdelta1 = self.hdu[0].header['CDELT1']
        cdelta2 = self.hdu[0].header['CDELT2']

        # number of pixels in a beam
        m = np.trunc(np.pi * (b1 * b2 / 2.) / (3600. * cdelta2) ** 2)

        cosdec = abs(np.cos(crval2 * np.pi / 180))
        dec = crval2 + (np.linspace(1, self.npixel1,
                                    self.npixel1) - crpix2) * cdelta2  # degrees
        ra = crval1 + (np.linspace(1, self.npixel1,
                                   self.npixel1) - crpix1) * cdelta1 / cosdec  # degrees

        # Coordinate pixel in gradi con origine sulla stella
        x = (-1) * (ra - ra0) * cosdec
        y = dec - dec0

        # Rotazione di coordinate insenso antiorario (rotazione oraria)
        x1 = np.zeros(shape=(self.npixel1, self.npixel1))
        y1 = np.zeros(shape=(self.npixel1, self.npixel1))
        v = range(self.npixel1)

        for i in v:
            for j in v:
                x1[i, j] = x[i] * (np.cos(pa)) + y[j] * (np.sin(pa))
                y1[i, j] = (-1.0) * x[i] * (np.sin(pa)) + y[j] * (np.cos(pa))

        if pa > 0:
            y1 = -1 * y1  # y1 asse maggiore diretto est-ovest

        # Linearizzazione array coordinate
        cosi = np.cos(incl)
        r = np.sqrt(pow(x1 / cosi, 2) + pow(y1, 2))
        xv = np.reshape(x1 * 3600 * dist,
                        np.shape(x1)[0] * np.shape(x1)[1])  # au
        yv = np.reshape(y1 * 3600 * dist,
                        np.shape(y1)[0] * np.shape(y1)[1])  # au
        rv = np.reshape(r * 3600 * dist,
                        np.shape(y1)[0] * np.shape(y1)[1])  # au
        fv = np.reshape(self.flux,
                        np.shape(self.flux)[0] * np.shape(self.flux)[1])
        indnodi = np.where((abs(xv) <= rismin * dist / 2.))

        # Medie anulari
        # step=rismin*dist/2.#au
        # bin=np.arange(xmin,xmax,step) #au
        if nodi:
            bin = np.concatenate([-binau, binau])
            tol = abs(bin[0] - bin[1]) / 2.  # au
            fmed = np.zeros(len(bin))
            err = np.copy(fmed)
            xx = yv[indnodi]
            yy = fv[indnodi]

            for k in xrange(len(bin)):
                amin = bin[k] - tol
                amax = bin[k] + tol
                ind = np.where((yv[indnodi] >= amin) & (yv[indnodi] <= amax))
                n = np.shape(fv[indnodi][ind])[0]
                fmed[k] = np.mean(fv[indnodi][ind])
                dd = min([n - 1, m])
                err[k] = np.std(fv[indnodi][ind], ddof=1)
        else:
            bin = np.copy(binau)
            tol = abs(bin[0] - bin[1]) / 2.  # au
            fmed = np.zeros(len(bin))
            err = np.copy(fmed)
            xx = np.copy(rv)
            yy = np.copy(fv)

            for k in xrange(len(bin)):
                amin = bin[k] - tol
                amax = bin[k] + tol
                indexe = np.where(((pow(yv, 2) + (
                pow(xv, 2) / pow(cosi, 2))) >= pow(max(0., amin), 2)) & ((
                                                                         pow(yv,
                                                                             2) + (
                                                                         pow(xv,
                                                                             2) / pow(
                                                                             cosi,
                                                                             2))) <= pow(
                    amax, 2)))
                n = len(fv[indexe])
                fmed[k] = np.mean(fv[indexe])
                dd = min([n - 1, m])
                err[k] = np.std(fv[indexe], ddof=1)

        if plotfilename:
            fig = plt.figure(figsize=(10, 7))
            gs = gridspec.GridSpec(3, 1)
            ax = plt.subplot(gs[:3, 0])
            # ax = plt.subplot()

            ax.tick_params(axis='y', labelsize=fonticks)
            ax.tick_params(axis='x', labelsize=fonticks)
            plt.rc('ytick', labelsize=fonticks)
            plt.rc('xtick', labelsize=fonticks)
            ax.xaxis.set_tick_params(width=1, length=5)
            ax.yaxis.set_tick_params(width=1, length=5)
            etiax = plt.gca().axes.get_yticklabels()
            # etiax[0].set_visible(False)

            plt.plot(xx, yy, 'b.', alpha=0.2)
            # plt.plot(bin,fmed,'k.', markersize=markers)
            plt.plot(bin, fmed, 'ko')
            plt.errorbar(bin, fmed, xerr=None, yerr=err, fmt=None, ecolor='k')

            ymax = fv.max() * 1.1
            # plt.yscale('log')
            ax.set_xlim(xmin, xmax)
            ax.set_ylim(0., ymax)

            plt.ylabel('I (Jy/beam)', fontsize=fontlab)
            plt.xlabel('r (AU)', fontsize=fontlab)
            plt.grid(False)
            # ax.yaxis.set_ticks_position('left')

            # Second x axis
            ax2 = ax.twiny()
            plt.xlim(xmin, xmax)
            ticks1b = np.arange(0., xmax / dist, 0.5) * dist
            ticks1a = -ticks1b[1:]
            ticks1a.sort()
            if not (nodi):
                ticks1a = []
            ticks1 = np.concatenate((ticks1a, ticks1b))
            newlabels = np.round(ticks1 / dist, 1)
            plt.xticks(ticks1, newlabels)
            labelissime = plt.gca().axes.get_xticklabels()
            s = np.shape(labelissime)[0]
            ax2.xaxis.set_tick_params(width=1, length=5)
            ax2.yaxis.set_tick_params(width=1, length=5)
            ax2.set_xlabel('r (arcsec)', fontsize=fontlab)

            fig.tight_layout(pad=4.5)
            plt.subplots_adjust(hspace=0.08)
            # plt.savefig(stri,bbox_inches='tight')
            plt.show()
            plt.savefig(plotfilename)
            # plt.close()

        return bin, fmed, err

