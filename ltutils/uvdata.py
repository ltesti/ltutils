__author__ = 'ltesti - 19 Oct 2018'

import numpy as np
import scipy.constants as sc
import scipy.stats as ss
import os
import matplotlib.pyplot as plt

from .star_data import StarData

try:
    from statsmodels.nonparametric import smoothers_lowess as sml
    have_sml = True
except:
    print('Warning: cannot load statsmodels.nonparametric')
    have_sml = False


class UVData(object):
    def __init__(self, infile, mystar='dummy', wl=None, doread=True, has_model=False, read_old=False):
        self.no_err = True
        if doread:
            self.no_err = self.__check_par_read_data(infile, wl=wl, read_old=read_old)
        if self.no_err:
            self.has_model = has_model
            self.__set_star_infile(mystar)
            self.__calc_uvd_amp_dep(in_klambda=True)
            self.binned = False
        else:
            print("Exiting with errors: uv data not properly loaded.")

    def __check_par_read_data(self,infile, wl=None, read_old=False):
        par_err = False
        try:
            self.infile = infile
            openfile = open(self.infile, 'r')
            openfile.close()
            if (read_old):
                if (wl==None):
                    print("Error: wl={0} missing (for old files it is necessary to pass the wavelength)".format(wl))
                    par_err = True
                else:
                    self.wl = wl           # This should be in m
        except:
            print("Error: cannot open file {0}".format(self.infile))
            par_err = True

        # note: now that I read/write the frequency, I should set up wl properly from the frequency
        if par_err:
            print("Exit without loading data...")
        else:
            if read_old:
                self.u, self.v, self.re, self.im, self.we, self.freqs, self.widths, self.spw, self.nchan = self.read_uv_from_ascii_file_old(self.infile)
            else:
                self.u, self.v, self.re, self.im, self.we, self.freqs, self.widths, self.spw, self.nchan = self.read_uv_from_ascii_file(self.infile)
            if (not par_err):
                self.wl=(sc.c / self.freqs)
        #
        return not par_err

    def __calc_uvd_amp_dep(self, in_klambda=True):
        #
        self.uvd = self.calc_uvd(self.u, self.v, self.wl, in_klambda=in_klambda)
        self.amp = self.calc_amp(self.re, self.im)
        if self.has_model:
            self.mod_amp = self.calc_amp(self.mod_re, self.mod_im)
        self.deproject(self.myStar.pa, self.myStar.inc)

    def __set_star_infile(self, sname):
        self.myStar = StarData(sname)

    def __str__(self):
        rep = "UVData for: " + self.myStar.name + ", read from ascii file: " + self.infile + ", wavelength: " + str(
            self.wl)

        return rep

    # reads from standard uvfile from disk (u,v,re,im,w)
    def read_uv_from_ascii_file_old(self, myasciifile):
        openfile = open(myasciifile, 'r')
        lines = openfile.readlines()
        openfile.close()

        size = len(lines)
        myu = np.zeros(size, dtype='float64')
        myv = np.zeros(size, dtype='float64')
        myre = np.zeros(size, dtype='float64')
        myim = np.zeros(size, dtype='float64')
        mywe = np.zeros(size, dtype='float64')
        for j, line in enumerate(lines):
            myu[j], myv[j], myre[j], myim[j], mywe[j] = line.split()[0:5]

        # extract maximum number of channels and number of uv points
        # note here I am assuming that all spw have the same nchan=maxch

        maxch = 1

        # define the structure of the vectors/arrays
        ff = sc.c / self.wl 

        re = np.zeros((maxch,size))
        im = np.zeros((maxch,size))
        spw = np.zeros((maxch,size))
        freqs = ff + np.ones((maxch,size))
        widths = -np.ones((maxch,size))
        nchan = []

        # fill in data
        re[0,:] = myre
        im[0,:] = myim
        for i in range(size):
            nchan.append(maxch)

        return myu, myv, re, im, mywe, freqs, widths, spw, nchan


    # reads from standard uvfile from disk (u,v,re,im,w)
    def read_uv_from_ascii_file(self, myasciifile):
        openfile = open(myasciifile, 'r')
        lines = openfile.readlines()
        openfile.close()

        while((lines[0].split())[0] == "#"):
            lines.pop(0)

        size = len(lines)
        myu = np.zeros(size, dtype='float64')
        myv = np.zeros(size, dtype='float64')
        myre = np.zeros(size, dtype='float64')
        myim = np.zeros(size, dtype='float64')
        mywe = np.zeros(size, dtype='float64')
        myfreqs = np.zeros(size, dtype='float64')
        mywidths = np.zeros(size, dtype='float64')
        myspw = np.zeros(size, dtype='int')
        mychan = np.zeros(size, dtype='int')
        for j, line in enumerate(lines):
            myu[j], myv[j], myre[j], myim[j], mywe[j], myfreqs[j], mywidths[j], myspw[j], mychan[j] = line.split()[0:9]

        # extract maximum number of channels and number of uv points
        # note here I am assuming that all spw have the same nchan=maxch
        maxch = int(np.max(mychan)+1)
        fnpoi = len(myu)/maxch
        npoi = int(fnpoi)
        if (float(npoi) != fnpoi):
            printf("Error reading file, number of lines not a multiple of nchan (nlines={0}, nchan={1})".format(len(myu),maxch))

        # define the structure of the vectors/arrays
        u = np.zeros((maxch,npoi))
        v = np.zeros((maxch,npoi))
        we = np.zeros((maxch,npoi))
        re = np.zeros((maxch,npoi))
        im = np.zeros((maxch,npoi))
        spw = np.zeros((maxch,npoi))
        freqs = np.zeros((maxch,npoi))
        widths = np.zeros((maxch,npoi))
        nchan = []

        # fill in data
        for i in range(npoi):
            for j in range(maxch):
                u[j,i] = myu[i*maxch]
                v[j,i] = myv[i*maxch]
                we[j,i] = mywe[i*maxch]
                spw[j,i] = myspw[i*maxch]
                re[j,i] = myre[i*maxch+j]
                im[j,i] = myim[i*maxch+j]
                freqs[j,i] = myfreqs[i*maxch+j]
                widths[j,i] = mywidths[i*maxch+j]
            #
            # note here I am assuming that all spw have the same nchan=maxch
            nchan.append(maxch)

        return u, v, re, im, we, freqs, widths, spw, nchan

    # Compute/set uvdistance, wle in the same units as (u,v)
    def calc_uvd(self, u, v, wl, in_klambda=True):
        factor = 1.
        if in_klambda:
            factor = 1.e3
        return np.sqrt(u * u + v * v) / wl / factor

    # Compute/set amplitude
    @staticmethod
    def calc_amp(re, im):
        return np.sqrt(re * re + im * im)

    # Compute average and dispersion of re and im
    # note: here re, im, we are arbitrary vecors, I did
    #       it this way to allow easy means on subsets
    #       another (better?) option would be to pass the
    #       indices of the re, im, we that we want to use
    @staticmethod
    def calc_avg(re, im, we, noweight=False):
        ws = we.sum()
        if ws > 0.:
            if noweight:
                rem = np.mean(re)
                imm = np.mean(im)
            else:
                rem = (re * we).sum() / ws
                imm = (im * we).sum() / ws
        else:
            rem = 0.
            imm = 0.
        erem = np.sqrt(((re - rem) * (re - rem)).sum()) / (np.prod(re.shape) - 1.)
        eimm = np.sqrt(((im - imm) * (im - imm)).sum()) / (np.prod(im.shape) - 1.)

        return rem, erem, imm, eimm

    # Compute binned re, im and amp
    # returns the binned arrays and std deviation in each bin
    def calc_binned(self, bin, dep=True):
        if dep:
            uvd = self.uvd_dep
        else:
            uvd = self.uvd
        db2 = (bin[1] - bin[0]) / 2.
        reb = np.zeros(len(bin))
        imb = np.zeros(len(bin))
        amb = np.zeros(len(bin))
        if self.has_model:
            mreb = np.zeros(len(bin))
            mimb = np.zeros(len(bin))
            mamb = np.zeros(len(bin))
            mereb = np.zeros(len(bin))
            meimb = np.zeros(len(bin))
            meamb = np.zeros(len(bin))
        ereb = np.zeros(len(bin))
        eimb = np.zeros(len(bin))
        eamb = np.zeros(len(bin))
        for i in xrange(len(bin)):
            idx = np.where((uvd > bin[i] - db2) & (uvd <= bin[i] + db2))
            reb[i], ereb[i], imb[i], eimb[i] = self.calc_avg(self.re[idx], self.im[idx], self.we[idx])
            if self.has_model:
                mreb[i], mereb[i], mimb[i], meimb[i] = self.calc_avg(self.mod_re[idx], self.mod_im[idx], self.we[idx])
                mamb[i] = np.sqrt(mreb[i] * mreb[i] + mimb[i] * mimb[i])
                meamb[i] = np.sqrt(mereb[i] * mereb[i] + meimb[i] * meimb[i])
            amb[i] = np.sqrt(reb[i] * reb[i] + imb[i] * imb[i])
            eamb[i] = np.sqrt(ereb[i] * ereb[i] + eimb[i] * eimb[i])

        self.binned = True
        self.mybin = bin
        self.reb = reb
        self.ereb = ereb
        self.imb = imb
        self.eimb = eimb
        self.amb = amb
        self.eamb = eamb
        if self.has_model:
            self.mreb = mreb
            self.mimb = mimb
            self.mamb = mamb
            self.mereb = mereb
            self.meimb = meimb
            self.meamb = meamb
            uv_binned = dict(reb=reb, ereb=ereb, imb=imb, eimb=eimb, amb=amb, eamb=eamb, mreb=mreb, mimb=mimb, mamb=mamb)
        else:
            uv_binned = dict(reb=reb, ereb=ereb, imb=imb, eimb=eimb, amb=amb, eamb=eamb)
        return uv_binned

    # Deproject visibilities for a position angle pa and inclination inc
    def deproject(self, pa, inc):

        if inc == 0.0:
            self.u_dep = np.copy(self.u)
            self.v_dep = np.copy(self.v)
            self.uvd_dep = np.copy(self.uvd)
        else:
            ur = self.u * np.cos(pa) - self.v * np.sin(pa)
            vr = self.u * np.sin(pa) + self.v * np.cos(pa)

            ur *= np.cos(inc)

            self.u_dep = ur * np.cos(-pa) - vr * np.sin(-pa)
            self.v_dep = ur * np.sin(-pa) + vr * np.cos(-pa)

            self.uvd_dep = self.calc_uvd(self.u_dep, self.v_dep, self.wl, in_klambda=True)

    def uvplot(self, yp="Real", uv_dep=False, binned=False, mycolor="blue", mycolor_mod="red"):
        plot_names = dict(real='real', re='real',
                          imag='imag', imaginary='imag', im='imag',
                          amp='amp', amplitude='amp')
        uvp = self.uvd
        if uv_dep:
            uvp = self.uvd_dep
        if binned:
            if self.binned:
                uvp = self.mybin
            else:
                print("Error! Data has not yet been binned, run self.calc_binned(bin) first!")
                print("Plotting non binned data...")
        if yp.lower() in plot_names:
            doplot = plot_names[yp.lower()]
            if doplot == "real":
                if binned & self.binned:
                    yp = self.reb
                    eyp = self.ereb
                    if self.has_model:
                        myp = self.mreb
                        meyp = self.mereb
                else:
                    yp = self.re
                    if self.has_model:
                        myp = self.mod_re
            if doplot == "imag":
                if binned & self.binned:
                    yp = self.imb
                    eyp = self.eimb
                    if self.has_model:
                        myp = self.mimb
                        meyp = self.meimb
                else:
                    yp = self.im
                    if self.has_model:
                        myp = self.mod_im
            if doplot == "amp":
                if binned & self.binned:
                    yp = self.amb
                    eyp = self.eamb
                    if self.has_model:
                        myp = self.mamb
                        meyp = self.meamb
                else:
                    yp = self.amp
                    if self.has_model:
                        myp = self.mod_amp
        else:
            print("Error! Variable {0} not recognized!".format(yp))
            print("Plotting amplitude...")
            if binned & self.binned:
                yp = self.amb
                eyp = self.eamb
                if self.has_model:
                    myp = self.mamb
                    meyp = self.meamb
            else:
                yp = self.amp
                if self.has_model:
                    myp = self.mod_amp
        if binned & self.binned:
            plt.errorbar(uvp, yp, yerr=eyp, fmt="o", color=mycolor)
            if self.has_model:
                plt.errorbar(uvp, myp, yerr=meyp, fmt='.', color=mycolor_mod, linestyle='solid')
        else:
            plt.plot(uvp, yp, ".", color=mycolor)
            if self.has_model:
                plt.plot(uvp, myp, color=mycolor_mod, fmt='.', linestyle='solid')

    @staticmethod
    def __fg(x, s):
        ff = np.exp(-x * x / (2. * s * s))
        return ff

    @staticmethod
    def __get_fit(x, y, my_is_sorted=True, my_delta=4., my_frac=0.05, my_it=3, my_return_sorted=False):
        #
        y_f = sml.lowess(y, x, is_sorted=my_is_sorted, delta=my_delta, frac=my_frac, it=my_it, return_sorted=my_return_sorted)
        #
        return y_f

    def __do_plot_weights(self, xp, re_f, im_f, re_r, re_std, im_r, im_std, plsig=1.6):
        #
        # Plot real and imaginary part
        # get uvd minmax for plot
        duvd = max(xp) - min(xp)
        uvp_min = min(xp) - 0.025*duvd
        uvp_max = max(xp) + 0.025*duvd
        #
        # Top panel is Real part uvplot
        plt.subplot(4, 1, 1)
        self.uvplot(yp='real', uv_dep=False)
        plt.plot(xp,re_f,color='green',linestyle='dashed',linewidth=2)
        plt.plot([0., 1.e9], [0., 0.], color='green', linestyle='dotted')
        plt.xlim(uvp_min, uvp_max)
        #
        # Middle panel is Imaginary part uvplot
        plt.subplot(4, 1, 2)
        self.uvplot(yp='imag', uv_dep=False)
        plt.plot(xp, im_f, color='green', linestyle='dashed', linewidth=2)
        plt.plot([0., 1.e9], [0., 0.], color='green', linestyle='dotted')
        plt.xlim(uvp_min, uvp_max)
        #
        # Bottom left is Real part histogram
        plt.subplot(2, 2, 3)
        self.__subplot_hist_gauss(re_r, re_std, pl_sig=plsig, mycolor='red')
        #
        # Bottom left is Imaginary part histogram
        plt.subplot(2, 2, 4)
        self.__subplot_hist_gauss(im_r, im_std, pl_sig=plsig, mycolor='blue')

    def __subplot_hist_gauss(self, v, std, pl_sig=1.6, mycolor='red'):
            dxp = pl_sig*std
            nn, xb, patches = plt.hist(v, 100, color=mycolor)
            plt.xlim(-dxp,dxp)
            xg = np.linspace(-dxp,dxp, 100)
            yy = self.__fg(xg, std)
            ff = (nn.sum() * (xb[1] - xb[0])) / (yy.sum() * (xg[1] - xg[0]))
            plt.plot(xg, ff * yy, linestyle="dashed", color='green', linewidth=2)

    #
    # Calcola la standard deviation usando un sigma clip
    #    sulla prima iterazione
    @staticmethod
    def __get_std(v, sigma_clip=2.0):
        std_0 = ss.tstd(v)
        dv = sigma_clip * std_0
        return ss.tstd(v, limits=(-dv, dv))

    def get_weight(self, doplot=True):
        #
        # Sort vectors
        re_sort = self.re.reshape(np.prod(self.re.shape))
        im_sort = self.im.reshape(np.prod(self.im.shape))
        uvd_sort = self.uvd.reshape(np.prod(self.uvd.shape))
        re_sort = np.copy(re_sort[np.argsort(uvd_sort)])
        im_sort = np.copy(im_sort[np.argsort(uvd_sort)])
        uvd_sort = np.copy(uvd_sort[np.argsort(uvd_sort)])
        #
        # Fit and subtract real and imaginary part
        delta_uvd = 0.01*(uvd_sort.max()-uvd_sort.min())
        if have_sml:
            re_f = self.__get_fit(uvd_sort, re_sort, my_delta=delta_uvd)
            im_f = self.__get_fit(uvd_sort, im_sort, my_delta=delta_uvd)
        else:
            print('Error: system does not have statmodels, not fitting the data values!')
            re_f = np.copy(re_sort - re_sort)
            im_f = np.copy(im_sort - im_sort)
        re_r = re_sort - re_f
        im_r = im_sort - im_f
        #
        #
        re_std = self.__get_std(re_r, sigma_clip=1.55)
        im_std = self.__get_std(im_r, sigma_clip=1.55)
        #
        # Plot proudly, if requested...
        if doplot:
            self.__do_plot_weights(uvd_sort, re_f, im_f, re_r, re_std, im_r, im_std, plsig=3.0)
        #
        # Calcolo dei fattori per i pesi
        wm = np.sum(self.we) / np.prod(self.we.shape)
        rat_re = 1. / (wm * re_std ** 2.)
        rat_im = 1. / (wm * im_std ** 2.)
        print("Ratios (1./std^2)/we: Real={0}  Imag={1}  Average={2}".format(rat_re, rat_im, (rat_re + rat_im) / 2.))

        return rat_re, rat_im

    def write_uv_to_ascii(self,outfile):
        #
        # Open the output file (Format will be: u,v,Re,Im,We,spw)
        nuv = self.u.shape[-1]
        
        with open(outfile, "w") as f:
            f.write("# u[m] v[m] Re[Jy] Im[Jy] weight freqs[Hz] chan_width[Hz] spw_id chan_id \n")
            for i in range(nuv):
                for ch_id in range(self.nchan[i]):
                    f.write("%+.15e  %+.15e  %+.10e  %+.10e  %+.10e %+.10e %+.10e %3d %5d\n" % (self.u[ch_id, i],self.v[ch_id, i],self.re[ch_id, i],self.im[ch_id, i],self.we[ch_id, i],self.freqs[ch_id, i],self.widths[ch_id, i],self.spw[i],ch_id))

    def __filename_mod_res(self, infile_root, suffix, file_extension, isres=False):
        #
        if isres:
            default_suff = '_res'
        else:
            default_suff = '_mod'
        if not infile_root:
            file_base = self.infile[:-3]
        else:
            file_base = infile_root
        if not suffix:
            modelfile_base = file_base+default_suff
        else:
            modelfile_base = file_base+suffix
        if not file_extension:
            modelfile = modelfile_base+'.txt'
        else:
            modelfile = modelfile_base+file_extension
        #
        return modelfile 

    # The three methods that follow are used to load the model and residual visibility tables
    # from ascii files, check the consistency with the MS table and write out the corresponding MS
    # A typical session would look like this:
    #
    #    import ltutils as ltu
    #    uvdata = ltu.UVDataMS(('my_visdata_ms',tb),)
    #    uvdata.read_mod_res(file_suffix)     # this will read and check file_suffix+'_mod.txt' and '_res.txt'
    #    uvdata.write_mod_res_ms()            # this writes file_suffix+'_mod.ms' and '_res.ms'
    #
    def read_mod_res(self, suffix_mod=None, suffix_res=None, infile_root=None, file_extension=None, calc_res=False, uvtol=1e-3):
        '''
        This function will read the model and residual files (in compatible format) if calc_res=False (default)

        calc_res = True only reads the model and computes the residual
        '''
        #
        has_mod = False
        has_res = False
        #
        # if not infile_root:
        #     self.file_base = self.infile
        # else:
        #     self.file_base = infile_root
        # if not suffix:
        #     self.modelfile_base = self.file_base+'_mod'
        # else:
        #     self.modelfile_base = self.file_base+suffix
        # if not file_extension:
        #     self.modelfile = self.modelfile_base+'.txt'
        # else:
        #     self.modelfile = self.modelfile_base+file_extension
        self.modelfile = self.__filename_mod_res(infile_root, suffix_mod, file_extension)
        print('Reading model data: {0}'.format(self.modelfile))
        #
        self.mod_u, self.mod_v, self.mod_re, self.mod_im, self.mod_we, self.mod_freqs, self.mod_widths, self.mod_spw, self.mod_nchan = self.read_uv_from_ascii_file(self.modelfile)
        has_mod = self.check_consistency(self.mod_u, self.mod_v, uvtol=uvtol)
        #if (has_res == False):
        #    print("Error:\n    the (uv) of {0} and of model ({1}) do not match within {2}m!".format(self.infile,self.modelfile,uvtol))
        #
        if has_mod:
            if calc_res:
                self.res_u = np.copy(self.mod_u)
                self.res_v = np.copy(self.mod_v)
                self.res_we = np.copy(self.mod_we)
                self.res_re = self.re - self.mod_re
                self.res_im = self.im - self.mod_im
                self.res_freqs = np.copy(self.mod_freqs)
                self.res_widths = np.copy(self.mod_widths)
                self.res_spw = np.copy(self.mod_spw)
                self.res_nchan = np.copy(self.mod_nchan)
                has_res = True
            else:
                self.resfile = self.__filename_mod_res(infile_root, suffix_res, file_extension, isres=True)
                self.res_u, self.res_v, self.res_re, self.res_im, self.res_we, self.res_freqs, self.res_widths, self.res_spw, self.res_nchan = self.read_uv_from_ascii_file(self.resfile)
                has_res = self.check_consistency(self.res_u, self.res_v, uvtol=uvtol, isres=True)
                #if (has_res == False):
                #    print("Error:\n    the (uv) of {0} and of model/residual ({1}) do not match within {2}m!".format(self.infile,self.resfile,uvtol))
        # 
        if (has_mod and has_res):
            self.has_mod_res = True
        else:
            self.has_mod_res = False

    def check_consistency(self,u,v,isres=False,uvtol=1e-3):
        #
        tol2 = uvtol**2
        #
        if isres:
            file_used = self.resfile
        else:
            file_used = self.modelfile
        # check vector length
        if (len(u) != len(self.u)):
            print("Error:\n    the legth of the data ms {0} and the model/residual (tables from {1}) are different!".format(self.infile,file_used))
            return False
        else:
            delta_u = u - self.u
            delta_v = v - self.v
            ndum=len(np.where(delta_u**2 > tol2)[0])
            ndvm=len(np.where(delta_v**2 > tol2)[0])
            if (ndum > 0 or ndvm > 0):
                print("Error:\n    the (uv) of {0} and of model/residual ({1}) do not match within {2}m!".format(self.infile,file_used,np.sqrt(tol2)))
                return False
        #
        return True




class UVDataMS(UVData):
    # Modifies the class UVData to read the data from a MS file
    # required to be in CASA and have access to the tb tool
    # Examples:
    #      cida1_cen = uvlt.UVDataMS(('cida1_2015.1.00934_cont_sc_cent.ms',tb), mystar='cida1')
    #      cida1_cen = uvlt.UVDataMS(('cida1_2015.1.00934_cont_sc_cent.ms',tb))   # default is mystar='dummy'

    def __init__(self, file_tuple, mystar='dummy', has_model=False):
        self.infile = file_tuple[0]
        self.tb = file_tuple[1]
        self.dualpol = True
        self.has_model = has_model
        if len(file_tuple) > 2:
            self.dualpol = file_tuple[2]
        self.__read_uv_from_ms_file()
        super(UVDataMS, self).__init__(file_tuple, mystar=mystar, doread=False, has_model=self.has_model)
        self.has_mod_res = False

    def __str__(self):
        rep = "UVData for: " + self.myStar.name + ", read from ms file: " + self.infile + ", wavelength: " + str(self.wl)

        return rep

    #
    # reads uvdata from MS file
    #
    def __read_uv_from_ms_file(self):

        #
        # Read columns from the ms file
        if self.has_model:
            #data, model, uvw, weight, self.spw, self.nchan, freqs, widths, self.spw_npoi = self.__read_ms_table()
            self.data, model, uvw, self.weight, self.spw, self.nchan, freqs, widths = self.__read_ms_table_old()
        else:
            #data, uvw, weight, self.spw, self.nchan, freqs, widths, self.spw_npoi = self.__read_ms_table()
            self.data, uvw, self.weight, self.spw, self.nchan, freqs, widths = self.__read_ms_table_old()

        #
        # Copy over the relevant column data into the appropriate np arrays
        # Takes care of dualpol data: note the two pols are averaged (weighted)
        #
        # The data matrix has as second axis the spectral channels
        #
        re_xx = np.array(self.data[0, :, :].real)
        im_xx = np.array(self.data[0, :, :].imag)
        if self.has_model:
            mod_re_xx = np.array(model[0, :, :].real)
            mod_im_xx = np.array(model[0, :, :].imag)
        wei_xx = np.array(self.weight[0, :])
        if self.dualpol:
            re_yy = self.data[1, :, :].real
            im_yy = self.data[1, :, :].imag
            wei_yy = self.weight[1, :]
            if self.has_model:
                mod_re_yy = np.array(model[1, :, :].real)
                mod_im_yy = np.array(model[1, :, :].imag)
        else:
            re_yy = self.data[0, :, :].real
            im_yy = self.data[0, :, :].imag
            wei_yy = self.weight[0, :]
            if self.has_model:
                mod_re_yy = np.array(model[0, :, :].real)
                mod_im_yy = np.array(model[0, :, :].imag)
        # Here we actually write the real, imaginary and weights into the arrays
        # Note that we have a safeguard for flagged data with we=0
        self.re = np.zeros(np.shape(re_xx))
        self.im = np.copy(self.re)
        self.mod_re = np.copy(self.re)
        self.mod_im = np.copy(self.re)
        self.we = np.zeros(np.shape(self.re))
        for i in range(len(self.re)):
            self.we[i,:] = (wei_xx + wei_yy)/2.
        nw_good = np.where(((wei_xx) != 0.0) | ((wei_yy) != 0.0))
        self.re[:,nw_good] = (re_xx[:,nw_good] * wei_xx[nw_good] + re_yy[:,nw_good] * wei_yy[nw_good]) / (
            wei_xx[nw_good] + wei_yy[nw_good])
        self.im[:,nw_good] = (im_xx[:,nw_good] * wei_xx[nw_good] + im_yy[:,nw_good] * wei_yy[nw_good]) / (
            wei_xx[nw_good] + wei_yy[nw_good])
        if self.has_model:
            self.mod_re[:,nw_good] = (mod_re_xx[:,nw_good] + mod_re_yy[:,nw_good])/2.
            self.mod_im[:,nw_good] = (mod_im_xx[:,nw_good] + mod_im_yy[:,nw_good])/2.

        # From the SPECTRAL_WINDOW table extract channel frequencies from CHAN_FREQ and
        # compute average freq, then convert to wavelength in m
        
        maxch = int(np.max(self.nchan))
        self.freqs = np.zeros((maxch,len(freqs))) 
        self.widths = np.zeros((maxch,len(freqs))) 
        for i in range(len(freqs)):
            self.freqs[:,i] = freqs[i]
            self.widths[:,i] = widths[i]
        self.wl = sc.c / self.freqs 

        # Compute the u, v, w vectors
        #print("Shaping: np.shape(self.re)={0}".format(np.shape(self.re)))
        self.u = np.zeros((np.shape(self.re)))
        self.v = np.zeros((np.shape(self.re)))
        self.w = np.zeros((np.shape(self.re)))

        for i in range(maxch):
            self.u[i,:] = np.array(uvw[0, :])
            self.v[i,:] = np.array(uvw[1, :])
            self.w[i,:] = np.array(uvw[2, :])


    #
    # This is the function that actually reads the columns from the MS file on disk
    def __read_ms_table_old(self):
        # Open ms table
        self.tb.open(self.infile)

        # Check if CORRECTED_DATA column is present:
        all_columns = self.tb.colnames()
        self.correct = False
        if 'CORRECTED_DATA' in all_columns:
            data = self.tb.getcol("CORRECTED_DATA")
            self.correct = True
            #print('CORRECTED DATA!!!')
        else:
            data = self.tb.getcol("DATA")

        #print(' shape(data)={0}'.format(np.shape(data)))

        if self.has_model:
            model = self.tb.getcol("MODEL_DATA")

        # Get column UVW, WEIGHT and DATA_DESC_ID (spw info!)
        uvw = self.tb.getcol("UVW")
        weight = self.tb.getcol("WEIGHT")
        spw = self.tb.getcol("DATA_DESC_ID")
        spec_tab = self.tb.getkeyword('SPECTRAL_WINDOW')[7:]
        self.tb.close()
        #
        # From the SPECTRA_WINDOW table, we extract the number of channels and 
        # the spectral frequency and width of each channel
        self.tb.open(spec_tab)
        spwname = self.tb.getvarcol("NAME")
        nchan = []
        freqs = []
        widths = []
        for i in range(len(spw)):
            nchan.append(self.tb.getvarcol("NUM_CHAN")['r'+str(spw[i]+1)][0])
            f = np.array(self.tb.getvarcol("CHAN_FREQ")['r'+str(spw[i]+1)])
            freqs.append(f.reshape(nchan[-1]))
            widths.append((np.array(self.tb.getvarcol("CHAN_WIDTH")['r'+str(spw[i]+1)])).reshape(nchan[-1]))

        # Close ms file
        self.tb.close()

        if self.has_model:
            return data, model, uvw, weight, spw, nchan, freqs, widths
        else:
            return data, uvw, weight, spw, nchan, freqs, widths

    #
    # This is the function that actually reads the columns from the MS file on disk
    def __read_ms_table(self):
        # Open ms table
        self.tb.open(self.infile)

        # Read all general parameters and data
        all_columns = self.tb.colnames()

        uvw = self.tb.getcol("UVW")
        weight = self.tb.getcol("WEIGHT")
        spw = self.tb.getcol("DATA_DESC_ID")
        spec_tab = self.tb.getkeyword('SPECTRAL_WINDOW')[7:]
        # Close spectral table
        self.tb.close()

        # Read spectral definition table
        self.tb.open(spec_tab)
        spwname = (self.tb.getvarcol("NAME")).keys()
        spw_npoi = []
        for ss in spwname:
            spw_npoi.append(len(np.where(spw == (int(ss[1:])-1))[0]))
        print("spw_npoi={0}".format(spw_npoi))
        nchan = []
        freqs = []
        widths = []
        for i in range(len(spw)):
            nchan.append(self.tb.getvarcol("NUM_CHAN")['r'+str(spw[i]+1)][0])
            f = np.array(self.tb.getvarcol("CHAN_FREQ")['r'+str(spw[i]+1)])
            freqs.append(f.reshape(nchan[-1]))
            widths.append((np.array(self.tb.getvarcol("CHAN_WIDTH")['r'+str(spw[i]+1)])).reshape(nchan[-1]))
        # Close spectral table
        self.tb.close()

        self.correct = False
        if 'CORRECTED_DATA' in all_columns:
            datakey = "CORRECTED_DATA"
            self.correct = True
        else:
            datakey = "DATA"

        # Open ms table
        self.tb.open(self.infile)
        
        data = []
        if self.has_model:
            model = []

        nread = 0
        for nn in spw_npoi:
            print("reading: startrow={0}  nrow={1}".format(nread,nn))
            data.append(self.tb.getcol(datakey,startrow  = nread,nrow = nn))
            print("read: shape(data)={0} ".format(np.shape(data[-1])))

            if self.has_model:
                model.append(self.tb.getcol("MODEL_DATA",startrow  = nread,nrow = nn))

            nread += nn

        # Close spectral table
        self.tb.close()

        if self.has_model:
            return data, model, uvw, weight, spw, nchan, freqs, widths, spw_npoi
        else:
            return data, uvw, weight, spw, nchan, freqs, widths, spw_npoi

    #
    # write uv-data to MS file using as template the
    # input file for the class definition
    #
    def write_uv_to_ms_file(self, new_ms):
        #
        #
        if new_ms == self.infile:
            print('Error: cannot overwrite original file, please give another name')
        else:
            # Create new table copying the reference one
            syscommand = 'rm -rf ' + new_ms
            os.system(syscommand)
            syscommand = 'cp -r ' + self.infile + ' ' + new_ms
            os.system(syscommand)

            # Open the new MS, read the current data from the reference table
            # data, uvw, weight, spw, nchan, freqs, widths = self.__read_ms_table()
            # copy the data from the original table
            data = self.data.copy()
            weight = self.weight.copy()

            #
            # Prepare columns with new data to be written in the table
            data[0, :, :] = self.re + 1j*self.im
            weight[0, :] = self.we
            if self.dualpol:
                data[1, :, :] = self.re + 1j*self.im
                weight[1, :] = self.we

            #
            # Write out the new data in the (copied) table columns
            self.tb.open(new_ms, nomodify=False)
            self.tb.putcol("DATA", data)
            self.tb.putcol("WEIGHT", weight)
            if self.correct is True:
                self.tb.putcol("CORRECTED_DATA", data)
            self.tb.flush()
            # Close table tools
            self.tb.close()

    def write_mod_res_ms(self):
        #
        if self.has_mod_res:
            temp_re = self.re
            temp_im = self.im
            #
            self.re = self.mod_re
            self.im = self.mod_im
            self.write_uv_to_ms_file(self.infile[:-3]+'_mod.ms')
            #
            self.re = self.res_re
            self.im = self.res_im
            self.write_uv_to_ms_file(self.infile[:-3]+'_res.ms')
            #
            self.re = temp_re
            self.im = temp_im
        else:
            printf("Error: you first need to read the model and residual visibilities with read_mod_res()")

