#!/usr/bin/env python
# coding=utf-8

"""
python distribute file
"""

from __future__ import (absolute_import, division, print_function,
                        unicode_literals, with_statement)

from setuptools import setup, find_packages


def requirements_file_to_list(fn="requirements.txt"):
    """read a requirements file and create a list that can be used in setup.

    """
    with open(fn, 'r') as f:
        return [x.rstrip() for x in list(f) if x and not x.startswith('#')]


setup(
    name="ltutils",
    version="0.1.0",
    packages=find_packages(),
    install_requires=requirements_file_to_list(),
    author="Leonardo Testi",
    author_email="ltesti@eso.org",
    description="Utilities for manipulation of interferometric visibilities.",
    long_description=open('README.rst').read(),
    # package_data={u'': [u'LICENSE.txt', u'AUTHORS.rst']},
    license="GPLv3",
    url="tbd",
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 2.7',
    ]
)