=========
ltutils
=========
Set of utilities to manipulate visibilities and images

 - add description
 - how it is organized
 - what it does
 - useful tools
 - examples?

Installation
------------

**ltutils** works on `Python` >=2.7 and >=3.6 and can be installed with:

.. code-block :: bash

    pip install pyProdigy

To make **ltutils** available in CASA, run from the shell:

.. code-block :: bash

    casa-pip install pyProdigy

where `casa-pip` is a tool that can be downloaded at `https://github.com/radio-astro-tools/casa-python <https://github.com/radio-astro-tools/casa-python>`_.

To upgrade **ltutils** to a newer version on your system, just run:

.. code-block :: bash

    pip install --upgrade pyProdigy
    
To upgrade **ltutils** inside CASA use the `--no-deps` option to prevent `casa-pip` from automatically upgrading `numpy` and `matplotlib` (which is not allowed inside CASA and will lead to errors):

.. code-block :: bash

    casa-pip install --upgrade --no-deps pyProdigy
    
**ltutils** has been tested on CASA versions >= 5.6.0.

License and Attribution
-----------------------
If you use **ltutils** for your publication, please acknowledge the Prodigy project

**ltutils** is free software licensed under the LGPLv3 License. For more details see the LICENSE.

Remember that **ltutils** is released without guarantees of accuracy, always check that what you do makes sense for the purpose you are doing it. Never blindly trust numbers out of a computer.

© Copyright 2017-2021 Leonardo Testi and contributors.

